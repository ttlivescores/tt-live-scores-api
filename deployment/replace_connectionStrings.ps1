﻿# replace_appsettings.ps1
param([Parameter(Mandatory=$True)][string]$config)

$configPath = "$env:APPVEYOR_BUILD_FOLDER\$config"

Write-Output "Loading config file from $configPath"
$xml = [xml](Get-Content $configPath)

ForEach($add in $xml.configuration.connectionStrings.add)
{
    Write-Output "Processing AppSetting key $($add.name)"

    $matchingEnvVar = [Environment]::GetEnvironmentVariable($add.name)

    if($matchingEnvVar)
    {
        Write-Output "Found matching environment variable for key: $($add.name)"
        Write-Output "Replacing value $($add.connectionString)  with $matchingEnvVar"

        $add.connectionString = $matchingEnvVar
    }
}

$xml.Save($configPath)