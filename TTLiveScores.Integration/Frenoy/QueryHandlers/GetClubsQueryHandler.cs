﻿using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Integration.Contracts.Models.Frenoy;
using TTLiveScores.Integration.Contracts.Queries;

namespace TTLiveScores.Integration.Frenoy.QueryHandlers
{
    public class ClubsQueryHandler : IQueryHandler<GetClubs, IEnumerable<Club>>
    {
        private readonly string _frenoyClubsFile;

        public ClubsQueryHandler(string frenoyClubsFile)
        {
            _frenoyClubsFile = frenoyClubsFile;
        }

        public IEnumerable<Club> Query(GetClubs query)
        {
            using (TextFieldParser parser = new TextFieldParser(_frenoyClubsFile))
            {
                parser.Delimiters = new string[] { ";" };
                while (true)
                {
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        yield break;
                    }
                    yield return new Club { Id = parts[0], Name = parts[1] };
                }
            }
        }
    }
}