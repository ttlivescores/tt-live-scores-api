﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Microsoft.VisualBasic.FileIO;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Integration.Contracts.Models.Frenoy;
using TTLiveScores.Integration.Contracts.Queries;

namespace TTLiveScores.Integration.Frenoy.QueryHandlers
{
    public class FixturesQueryHandlers : IQueryHandler<GetFixtures, IEnumerable<Fixture>>
    {
        private readonly string _fixturesFilePath;

        public FixturesQueryHandlers(string fixturesFilePath)
        {
            _fixturesFilePath = fixturesFilePath;
        }

        public IEnumerable<Fixture> Query(GetFixtures query)
        {
            using (TextFieldParser parser = new TextFieldParser(_fixturesFilePath))
            {
                parser.Delimiters = new[] { ";" };
                while (true)
                {
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        yield break;
                    }
                    yield return new Fixture
                    {
                        MatchId = parts[0],
                        Level = ((Levels)int.Parse(parts[1])),
                        Category = ((Categories)int.Parse(parts[2])),
                        MatchNr = parts[3],
                        Date = parts[4] == "-" ? (DateTime?) null : DateTime.Parse(parts[4],new CultureInfo("nl-BE")),
                        HomeTeamClub = parts[5],
                        HomeTeamLetter = parts[6],
                        AwayTeamClub = parts[7],
                        AwayTeamLetter = parts[8],
                        LeagueId = int.Parse(parts[11]),
                        LeagueNumber = int.Parse(parts[13]),
                        LeagueLetter = parts[14]

                    };
                }
            }
        }
    }
}