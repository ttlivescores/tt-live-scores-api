﻿using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using TTLiveScores.Application.Contracts.Events;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Application.Features.Lineup.Events;
using TTLiveScores.Application.Infrastructure.SignalR;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;

namespace TTLiveScores.Web.Hubs
{
    public class WebsocketNotificationEventHandler : IEventHandler<ScoreChanged>
    , IEventHandler<LineupChanged>
    {
        private readonly IQueryService _queryService;

        public WebsocketNotificationEventHandler(IQueryService queryService)
        {
            _queryService = queryService;
        }

        public void Handle(ScoreChanged evt, EventMetaData eventMetaData)
        {
            var updateUser = GetUpdateUser(eventMetaData);
            foreach (var activeScoresSocket in ActiveScoresSockets.Sockets)
            {
                activeScoresSocket.SendScoreChanged(evt, updateUser);
            }
        }

        public void Handle(LineupChanged evt, EventMetaData eventMetaData)
        {
            
            var updateUser = GetUpdateUser(eventMetaData);
            foreach (var activeScoresSocket in ActiveScoresSockets.Sockets)
            {
                activeScoresSocket.SendLineupChanged(evt, updateUser);
            }
        }

        private string GetUpdateUser(EventMetaData eventMetaData)
        {
            var lastupdateuserId = eventMetaData.GetUserId();
            if (lastupdateuserId == null) return null;

            var updateUsers = _queryService.Query<GetUserNamesByIds, IDictionary<string, string>>(new GetUserNamesByIds(new[] { lastupdateuserId }));
            return updateUsers.ContainsKey(lastupdateuserId) ? updateUsers[lastupdateuserId] : null;
        }
    }
}