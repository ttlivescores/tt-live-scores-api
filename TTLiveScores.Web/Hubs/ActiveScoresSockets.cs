﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Owin.WebSocket;
using Serilog;
using TTLiveScores.Application.Contracts.Events;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Application.Features.Lineup.Events;
using TTLiveScores.Application.Infrastructure.SignalR;
using TTLiveScores.Web.DI;

namespace TTLiveScores.Web.Hubs
{
    public class ActiveScoresSockets : WebSocketConnection
    {
        public static IList<ActiveScoresSockets> Sockets = new List<ActiveScoresSockets>();

        private readonly List<string> _subscribedMatches = new List<string>();
        public override async Task OnMessageReceived(ArraySegment<byte> message, WebSocketMessageType type)
        {
            Log.Debug($"OnMessageReceived Starting");
            //Handle the message from the client

            //Example of JSON serialization with the client
            var json = Encoding.UTF8.GetString(message.Array, message.Offset, message.Count);

            var subscriptionMessage = JsonConvert.DeserializeObject<MatchSubscriptionMessage>(json);
            await HandleMatchSubscriptions(subscriptionMessage);

            Log.Debug($"OnMessageReceived Finished");
        }

        private async Task HandleMatchSubscriptions(MatchSubscriptionMessage message)
        {
            if (message?.Matches == null) return;

            var queryService = CompositionRoot.Instance.QueryService;

            var matchScores =
                queryService.Query<GetMatchProgesses, IEnumerable<MatchProgress>>(
                    new GetMatchProgesses(message.Matches)).Select(x => new { UpdateUserId = x.LastUpdateUserId, Event = new ScoreChanged(x.Id, x.HomeScore, x.AwayScore) });

            foreach (var matchId in message.Matches.Where(matchId => !_subscribedMatches.Contains(matchId)))
            {
                _subscribedMatches.Add(matchId);
            }

            var updateUserIds = matchScores.Where(x => !string.IsNullOrEmpty(x.UpdateUserId)).Select(x => x.UpdateUserId).Distinct();
            var updateUserNames = queryService.Query<GetUserNamesByIds, IDictionary<String, string>>(new GetUserNamesByIds(updateUserIds));

            foreach (var scoreChanged in matchScores)
            {
                var userName = !string.IsNullOrWhiteSpace(scoreChanged.UpdateUserId)?  
                        updateUserNames.ContainsKey(scoreChanged.UpdateUserId)
                    ? updateUserNames[scoreChanged.UpdateUserId]
                    : null
                    : null;
                await SendScoreChanged(scoreChanged.Event, userName);
            }

            Log.Debug($"HandleMatchSubscriptions Finished");
        }

        public async Task SendScoreChanged(ScoreChanged scoreChanged, string updateUser)
        {
            var message = new SignalREvent(scoreChanged, updateUser);
            var json = JsonConvert.SerializeObject(message);

            var toSend = Encoding.UTF8.GetBytes(json);
            await SafeSend(scoreChanged.MatchId, toSend);
        }

        public async Task SendLineupChanged(LineupChanged lineupChanged, string updateUser)
        {
            var message = new SignalREvent(lineupChanged, updateUser);
            var json = JsonConvert.SerializeObject(message);

            var toSend = Encoding.UTF8.GetBytes(json);
            await SafeSend(lineupChanged.MatchId, toSend);
        }

        public async Task SafeSend(string matchId, byte[] message)
        {
            if (!_subscribedMatches.Contains(matchId)) return;

            try
            {
                await this.SendText(message, true);
            }
            catch (Exception ex)
            {
                Log.Warning($"Closing websocket because of exception: {ex.Message}");
                await this.Close(WebSocketCloseStatus.InternalServerError, ex.Message);
            }
        }

        public override void OnOpen()
        {
            if (!Sockets.Contains(this))
                Sockets.Add(this);

            //send ack message
            var json = JsonConvert.SerializeObject(new { EventType = "SocketOpened" });
            var toSend = Encoding.UTF8.GetBytes(json);
            this.SendText(toSend, true);
        }

        public override void OnReceiveError(Exception error)
        {
            Log.Warning($"Error in websocket because of exception: {error.Message} {error.StackTrace}");
            base.OnReceiveError(error);
        }

        public override void OnClose(WebSocketCloseStatus? closeStatus, string closeStatusDescription)
        {
            Log.Warning($"Closing websocket. status: {closeStatus}, reason: {closeStatusDescription}");
            if (Sockets.Contains(this))
                Sockets.Remove(this);
        }
        //public override bool Authenticate(IOwinRequest request){return true;}

        public class MatchSubscriptionMessage
        {
            public string Message { get; set; }

            public List<string> Matches { get; set; }
        }

        
    }
}