﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using AggregateSource;
using LiteDB;
using Microsoft.Owin.Security.Provider;
using NEventStore;
using NEventStore.Persistence.Sql.SqlDialects;
using Serilog;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Features.Players;
using TTLiveScores.Application.Helpers;
using TTLiveScores.Application.Infrastructure.Eventstore;
using TTLiveScores.Application.Infrastructure.Eventstore.PollingClient;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Application.Infrastructure.Logging;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Controllers;
using ILogger = TTLiveScores.Framework.Infrastructure.ILogger;

namespace TTLiveScores.Web.DI
{
    public class CompositionRoot : IHttpControllerActivator
    {
        private static CompositionRoot _instance;
        public static CompositionRoot Instance => _instance ?? (_instance = new CompositionRoot());

        private static string _appDataPath = HttpContext.Current.Server.MapPath("App_Data");

        private IEventBus _eventBus;
        private IStoreEvents _eventStore;
        private LiteDatabase _liteDatabase;
        private ILogger _logger;
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly IEncryptionService _encryptionService;

        private ProjectionService _projectionService;

        CompositionRoot()
        {
            _eventStore = Wireup.Init().UsingSqlPersistence("EventStore") // Connection string is in app.config
                          .WithDialect(new MsSqlDialect())
                          .InitializeStorageEngine()
                          .UsingJsonSerialization()
                          .Compress()
                          .LogToOutputWindow()
                          .Build();

            _liteDatabase = CreateLiteDatabase();

            var seriLog = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger();
            
            _logger = new SerilogLoggerAdapter(seriLog);
            Log.Logger = seriLog;

            _encryptionService = new Md5EncryptionService();
            _queryService = new QueryServiceBootstrapper(CreateLiteDatabase).Setup();

            _eventBus = new EventBusBootstrapper(_logger, CreateLiteDatabase, _queryService).Setup();
            _commandBus = new CommandBusBootstrapper(_eventStore, _logger, _queryService, CreateLiteDatabase, _eventBus, _encryptionService).Setup();
        }

        public ILogger Logger => _logger;
        public IQueryService QueryService => _queryService;

        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor,
            Type controllerType)
        {
            if (controllerType == typeof(ScoresController))
            {
                return new ScoresController(_commandBus, _queryService);
            }
            if (controllerType == typeof(ClubsController))
            {
                return new ClubsController(_commandBus, _queryService);
            }
            if (controllerType == typeof(DivisionsController))
            {
                return new DivisionsController(_commandBus, _queryService);
            }
            if (controllerType == typeof(TeamsController))
            {
                return new TeamsController(_commandBus, _queryService);
            }
            if (controllerType == typeof(MatchesController))
            {
                return new MatchesController(_commandBus, _queryService);
            }
            if (controllerType == typeof(UserSettingsController))
            {
                return new UserSettingsController(_commandBus, _queryService, _logger);
            }
            if (controllerType == typeof(EventsController))
            {
                return new EventsController(new EventsRepository(_eventStore, _logger));
            }
            if (controllerType == typeof(PlayersController))
            {
                return new PlayersController(_commandBus, _queryService);
            }

            throw new ArgumentException("Unexpected type!", nameof(controllerType));
        }

        public ProjectionService CreateProjectionService()
        {
            var commitObserver = new EventBusDispatchCommitObserver(_eventBus, _logger);
            return new ProjectionService(new PollingClientAdapter(_eventStore, commitObserver, _logger));
        }

        private LiteDatabase CreateLiteDatabase()
        {
            var dbPath = _appDataPath + "\\projectionsDb.db";

            BsonMapper.Global.Entity<Team>()
               .DbRef(p => p.Club, DbLiteCollections.Clubs)
               .DbRef(p => p.Division, DbLiteCollections.Divisions);

            BsonMapper.Global.Entity<Match>()
                .DbRef(m => m.HomeTeam, DbLiteCollections.Teams)
                .DbRef(m => m.AwayTeam, DbLiteCollections.Teams)
                .DbRef(m => m.Division, DbLiteCollections.Divisions)
                ;

            BsonMapper.Global.Entity<UserSettings>()
                .Id(u => u.UserId)
                .Index(k=>k.SyncServiceKey);

            BsonMapper.Global.Entity<Player>()
                .DbRef(p => p.Club, DbLiteCollections.Clubs)
                .Index(p=>p.FullName);

            return new SeasonDatabase(dbPath);
        }
    }
}