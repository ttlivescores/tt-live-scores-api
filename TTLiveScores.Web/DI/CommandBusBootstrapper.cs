﻿using System;
using System.Collections.Generic;
using AggregateSource;
using FluentValidation;
using LiteDB;
using NEventStore;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Commands.Lineups;
using TTLiveScores.Application.Contracts.Commands.UserSettings;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Features.Clubs;
using TTLiveScores.Application.Features.Divisions;
using TTLiveScores.Application.Features.Lineup;
using TTLiveScores.Application.Features.MatchScores;
using TTLiveScores.Application.Features.MatchScores.Validators;
using TTLiveScores.Application.Features.Players;
using TTLiveScores.Application.Features.Players.Commands;
using TTLiveScores.Application.Features.Teams;
using TTLiveScores.Application.Features.UserSettings;
using TTLiveScores.Application.Features.UserSettings.Commands;
using TTLiveScores.Application.Helpers;
using TTLiveScores.Application.Infrastructure.Bus;
using TTLiveScores.Application.Infrastructure.Eventstore;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using MatchProgress = TTLiveScores.Application.Features.MatchScores.Domain.MatchProgress;
using TTLiveScores.Application.Features.Matches.Commands;

namespace TTLiveScores.Web.DI
{
    public class CommandBusBootstrapper
    {
        private readonly IStoreEvents _eventStore;
        private readonly ILogger _logger;
        private readonly IQueryService _queryService;
        private readonly Func<LiteDatabase> _liteDbFactory;
        private readonly IEventBus _eventBus;
        private readonly IEncryptionService _encryptionService;

        public CommandBusBootstrapper(IStoreEvents storeEvents, ILogger logger, IQueryService queryService, Func<LiteDatabase> liteDbFactory, IEventBus eventBus, IEncryptionService encryptionService)
        {
            _eventStore = storeEvents;
            _logger = logger;
            _queryService = queryService;
            _liteDbFactory = liteDbFactory;
            _eventBus = eventBus;
            _encryptionService = encryptionService;
        }

        public ICommandBus Setup()
        {
            var bus = new SimpleCommandBus();
            bus.Register(CreateMatchProgressHandler<UpdateScore>);
            bus.Register(CreateMatchProgressHandler<ClearMatchProgresses>);
            bus.Register(CreateLineupHandler<FillTeamLineup>);
            bus.Register(CreateClubsHandler<SyncClubs>);
            bus.Register(CreateDivisionsHandler<SyncDivisions>);
            bus.Register(CreateDivisionsHandler<ClearDivisions>);
            bus.Register(CreateTeamsHandler<SyncTeams>);
            bus.Register(CreateTeamsHandler<ClearTeams>);
            bus.Register(CreatePlayersHandler<AddPlayer>);
            bus.Register(CreatePlayersHandler<ClearPlayers>);
            bus.Register(CreateMatchesHandler<SyncMatches>);
            bus.Register(CreateMatchesHandler<ClearMatches>);
            bus.Register(CreateUserSettingsHandler<ChangeFavoriteClub>);
            bus.Register(CreateUserSettingsHandler<RequestSyncServiceKey>);
            bus.Register(CreateUserSettingsHandler<ChangePlayerId>);
            bus.Register(CreateUserSettingsHandler<FillUserName>);

            var authorizedCommandBus = new AuthorisedCommandBusDecorator(bus);
            authorizedCommandBus.Register(CreateMatchProgressAuthorizationHandler<UpdateScore>);

            return new ValidatedCommandBus(authorizedCommandBus, GetCommandValidators());
        }

        private IEnumerable<IValidator> GetCommandValidators()
        {
            return new[] { new UpdateScoreValidator(), };
        }

        private ICommandHandler<TCommand> CreateMatchProgressHandler<TCommand>() where TCommand : ICommand
        {
            var unitOfWork = new UnitOfWork();
            var repository = InitRepository<MatchProgress>(unitOfWork);

            var handler = (ICommandHandler<TCommand>)new MatchProgressCommandHandlers(repository, _queryService);

            return CreateEventStoreCommitHandler(handler, unitOfWork);
        }

        
        private ICommandAuthorizationHandler<TCommand> CreateMatchProgressAuthorizationHandler<TCommand>() where TCommand : ICommand
        {
            return (ICommandAuthorizationHandler<TCommand>)new MatchProgressCommandAuthorizationHandlers(_queryService);
        }

        private ICommandHandler<TCommand> CreateLineupHandler<TCommand>() where TCommand : ICommand
        {
            var lineupsRepository = new LiteDbRepository<MatchTeamLineup>(_liteDbFactory(), DbLiteCollections.Lineups);
            return (ICommandHandler<TCommand>)new LineupCommandHandlers(lineupsRepository, _queryService, new EventsRepository(_eventStore, _logger));
        }

        private ICommandHandler<TCommand> CreatePlayersHandler<TCommand>() where TCommand : ICommand
        {
            var playersRepository = new LiteDbRepository<Player>(_liteDbFactory(), DbLiteCollections.Players);
            return (ICommandHandler<TCommand>)new PlayerCommandHandler(playersRepository);
        }

        private ICommandHandler<TCommand> CreateClubsHandler<TCommand>() where TCommand : ICommand
        {
            var clubRepository = new LiteDbRepository<Club>(_liteDbFactory(), DbLiteCollections.Clubs);
            return (ICommandHandler<TCommand>)new ClubsCommandHandlers(_queryService, clubRepository);
        }

        private ICommandHandler<TCommand> CreateDivisionsHandler<TCommand>() where TCommand : ICommand
        {
            var divisionRepository = new LiteDbRepository<Division>(_liteDbFactory(), DbLiteCollections.Divisions);
            return (ICommandHandler<TCommand>)new DivisionsCommandHandlers(_queryService, divisionRepository);
        }

        private ICommandHandler<TCommand> CreateTeamsHandler<TCommand>() where TCommand : ICommand
        {
            var teamRepository = new LiteDbRepository<Team>(_liteDbFactory(), DbLiteCollections.Teams);
            return (ICommandHandler<TCommand>)new TeamCommandHandlers(_queryService, teamRepository);
        }

        private ICommandHandler<TCommand> CreateMatchesHandler<TCommand>() where TCommand : ICommand
        {
            var matchesRepository = new LiteDbRepository<Match>(_liteDbFactory(), DbLiteCollections.Matches);
            return (ICommandHandler<TCommand>)new MatchCommandHandlers(_queryService, matchesRepository);
        }


        private ICommandHandler<TCommand> CreateUserSettingsHandler<TCommand>() where TCommand : ICommand
        {
            var userSettingsRepository = new LiteDbRepository<UserSettings>(_liteDbFactory(), DbLiteCollections.UserSettings);
            var eventRepository = new EventsRepository(_eventStore, _logger);
            return (ICommandHandler<TCommand>)new UserSettingsCommandHandler(eventRepository, userSettingsRepository, _encryptionService);
        }

        private ICommandHandler<TCommand> CreateEventStoreCommitHandler<TCommand>(ICommandHandler<TCommand> innerHandler, UnitOfWork unitOfWork) where TCommand : ICommand
        {
            var eventRepository = new EventsRepository(_eventStore, _logger);
            return new EventStoreCommitHandler<TCommand>(innerHandler, unitOfWork, eventRepository, _logger);
        }

        private Framework.Eventstore.IRepository<TAggregate> InitRepository<TAggregate>(UnitOfWork unitOfWork) where TAggregate : class, IAggregateRootEntity, new()
        {
            return new AggregateSourceRepositoryAdapter<TAggregate>(
                 new AggregateSource.NEventStore.Repository<TAggregate>(() => new TAggregate(), unitOfWork,
                     _eventStore));
        }
    }
}