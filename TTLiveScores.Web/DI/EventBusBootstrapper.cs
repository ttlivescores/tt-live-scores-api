﻿using System;
using System.Web;
using LiteDB;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NEventStore;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Events;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Features.Lineup.Events;
using TTLiveScores.Application.Features.MatchScores;
using TTLiveScores.Application.Infrastructure.Bus;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Hubs;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace TTLiveScores.Web.DI
{
    public class EventBusBootstrapper
    {
        private readonly ILogger _logger;
        private readonly Func<LiteDatabase> _db;
        private readonly IQueryService _queryService;

        public EventBusBootstrapper(ILogger logger, Func<LiteDatabase> db, IQueryService queryService)
        {
            _logger = logger;
            _db = db;
            _queryService = queryService;
        }

        public IEventBus Setup()
        {
            var bus = new SimpleEventBus(_logger);

            
            var serializerSettings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            serializerSettings.ContractResolver = new DefaultContractResolver();
            
            var serializer = JsonSerializer.Create(serializerSettings);
            GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);

            bus.Register<ScoreChanged>((scorechanged, metaData) => new MatchProgressProjections(InitProjectionsRepository<MatchProgress>(DbLiteCollections.MatchProgress),_queryService).Handle(scorechanged, metaData));
            bus.Register<ScoreCleared>((scorechanged, metaData) => new MatchProgressProjections(InitProjectionsRepository<MatchProgress>(DbLiteCollections.MatchProgress),_queryService).Handle(scorechanged, metaData));
            
            bus.Register<ScoreChanged>((scorechanged, metaData) => new WebsocketNotificationEventHandler(_queryService).Handle(scorechanged, metaData));
            bus.Register<LineupChanged>((lineupchanged, metaData) => new WebsocketNotificationEventHandler(_queryService).Handle(lineupchanged, metaData));
            
            return bus;
        }

        private IProjectionRepository<TEntity> InitProjectionsRepository<TEntity>(string collectionName) where TEntity : new()
        {
            return new LiteDbRepository<TEntity>(_db(), collectionName);
        }
        
    }
}