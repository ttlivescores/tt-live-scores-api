﻿using System;
using System.Collections.Generic;
using System.Web;
using LiteDB;
using TTLiveScores.Application.Contracts.Lineups;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Application.Features.Clubs;
using TTLiveScores.Application.Features.Divisions;
using TTLiveScores.Application.Features.Frenoy;
using TTLiveScores.Application.Features.Frenoy.Players;
using TTLiveScores.Application.Features.Lineup;
using TTLiveScores.Application.Features.MatchScores;
using TTLiveScores.Application.Features.Players;
using TTLiveScores.Application.Features.Players.Queries;
using TTLiveScores.Application.Features.Teams;
using TTLiveScores.Application.Features.UserSettings;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Integration.Frenoy.QueryHandlers;

namespace TTLiveScores.Web.DI
{
    public class QueryServiceBootstrapper
    {
        private readonly Func<LiteDatabase> _liteDb;

        public QueryServiceBootstrapper(Func<LiteDatabase> liteDb)
        {
            _liteDb = liteDb;
        }

        public IQueryService Setup()
        {
            var service = new QueryService();
            
            service.Register<GetClubMatchProgress, IEnumerable<MatchProgress>>(() => new MatchProgressQueryHandlers(_liteDb()));
            service.Register<GetMatchProgesses, IEnumerable<MatchProgress>>(() => new MatchProgressQueryHandlers(_liteDb()));
            service.Register<GetAllMatchProgesses, IEnumerable<MatchProgress>>(() => new MatchProgressQueryHandlers(_liteDb()));

            service.Register(() => new ClubQueryHandlers(_liteDb()));
            service.Register(() => new DivisionQueryHandlers(_liteDb()));
            service.Register(() => new TeamQueryHandlers(_liteDb()));

            //Matches
            service.Register<GetAllMatches, IEnumerable<Match>>(() => new MatchQueryHandlers(_liteDb()));
            service.Register<GetActiveMatches, IEnumerable<Match>>(() => new MatchQueryHandlers(_liteDb()));
            service.Register<GetClubMatches, IEnumerable<Match>>(() => new MatchQueryHandlers(_liteDb()));
            service.Register<GetMatch, Match>(() => new MatchQueryHandlers(_liteDb()));

            //Lineups
            service.Register<GetLineups, IEnumerable<MatchTeamLineup>>(()=>new LineupQueryHandlers(_liteDb()));
            service.Register<GetLineup, MatchTeamLineup>(()=>new LineupQueryHandlers(_liteDb()));

            //Players
            service.Register<GetPlayers, IEnumerable<Player>>(() => new PlayerQueryHandlers(_liteDb()));
            service.Register<GetPlayersByName, IEnumerable<Player>>(() => new PlayerQueryHandlers(_liteDb()));
            service.Register<GetPlayerById, Player>(() => new PlayerQueryHandlers(_liteDb()));
            service.Register<GetPlayersById, IEnumerable<Player>>(() => new PlayerQueryHandlers(_liteDb()));

            //User settings
            service.Register<GetUserClub, string>(() => new UserSettingsQueryHandler(_liteDb()));
            service.Register<GetUserIdForSyncServiceKey, string>(() => new UserSettingsQueryHandler(_liteDb()));
            service.Register<GetUserSettings, UserSettings>(() => new UserSettingsQueryHandler(_liteDb()));
            service.Register<GetUsersWithSyncScoresLicenses, IEnumerable<UserSettings>>(() => new UserSettingsQueryHandler(_liteDb()));
            service.Register<GetUserNamesByIds, IDictionary<string, string>>(() => new UserSettingsQueryHandler(_liteDb()));


            // Frenoy queries
            service.Register(() => new ClubsQueryHandler(HttpContext.Current.Server.MapPath("~/App_Data/Frenoy/clubs.txt")));
            service.Register(() => new FixturesQueryHandlers(HttpContext.Current.Server.MapPath("~/App_Data/Frenoy/fixtures.txt")));
            service.Register(() => new FrenoyPlayersQueryHandlers(HttpContext.Current.Server.MapPath("~/App_Data/Frenoy/players.txt")));

            return service;
        }
    }
}