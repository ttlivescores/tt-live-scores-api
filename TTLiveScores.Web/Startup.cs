﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Cors;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using Owin.WebSocket.Extensions;
using TTLiveScores.Web.Hubs;

[assembly: OwinStartup(typeof(TTLiveScores.Web.Startup))]

namespace TTLiveScores.Web
{

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(MyCorsOptions.Value);
            //app.Map("/signalr", map =>
            //{
            //    map.UseCors(MyCorsOptions.Value);
            //    map.RunSignalR(new HubConfiguration());
            //});

            ConfigureAuth(app);
            StartProjections(app);
            //app.MapSignalR();
            app.MapWebSocketRoute<ActiveScoresSockets>("/wsscores");

        }

        private static readonly Lazy<CorsOptions> MyCorsOptions = new Lazy<CorsOptions>(() =>
        {
            return new CorsOptions
            {
                PolicyProvider = new CorsPolicyProvider
                {
                    PolicyResolver = context =>
                    {
                        var policy = new CorsPolicy
                        {
                            AllowAnyOrigin = true,
                            AllowAnyMethod = true,
                            AllowAnyHeader = true,
                            SupportsCredentials = false,
                            PreflightMaxAge = 86400
                        };
                        return Task.FromResult(policy);
                    }
                }
            };
        });
    }
}
