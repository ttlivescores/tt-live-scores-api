﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin;
using Microsoft.Owin.Extensions;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.Jwt;
using Owin;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Web.Security
{
    public static class SyncServiceHeaderAuthentication
    {
        public static IAppBuilder UseSyncServicekeyAuthentication(this IAppBuilder app,
            SyncServiceAuthenticationOptions options, IQueryService queryService)
        {
            if (app == null)
                throw new ArgumentNullException("app");
            app.Use(typeof(SyncServiceAuthenticationMiddleware), options, queryService);
            app.UseStageMarker(PipelineStage.Authenticate);
            return app;
        }
    }

    public class SyncServiceAuthenticationMiddleware : AuthenticationMiddleware<SyncServiceAuthenticationOptions>
    {
        private readonly IQueryService _queryService;

        public SyncServiceAuthenticationMiddleware(OwinMiddleware next, SyncServiceAuthenticationOptions options, IQueryService queryService) : base(next, options)
        {
            _queryService = queryService;
        }

        protected override AuthenticationHandler<SyncServiceAuthenticationOptions> CreateHandler()
        {
            return new SyncServiceAuthenticationHandler(_queryService);
        }
    }

    public class SyncServiceAuthenticationHandler : AuthenticationHandler<SyncServiceAuthenticationOptions>
    {
        private readonly IQueryService _queryService;

        public SyncServiceAuthenticationHandler(IQueryService queryService)
        {
            _queryService = queryService;
        }
        protected override async Task<AuthenticationTicket> AuthenticateCoreAsync()
        {
            string authorization = this.Request.Headers.Get("Authorization");

            string requestToken = null;
            if (!string.IsNullOrEmpty(authorization) && authorization.StartsWith("SyncService ", StringComparison.OrdinalIgnoreCase))
                requestToken = authorization.Substring("SyncService ".Length).Trim();

            if (string.IsNullOrWhiteSpace(requestToken)) return null;

            var userId = await Task.Run(() => GetUserId(requestToken));

            if (userId == null)
                return null;

            AuthenticationProperties authProperties = new AuthenticationProperties();
            authProperties.IssuedUtc = DateTime.UtcNow;
            authProperties.ExpiresUtc = DateTime.UtcNow.AddDays(1);
            authProperties.AllowRefresh = true;
            authProperties.IsPersistent = true;

            IList<Claim> claimCollection = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, userId),
                };
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claimCollection, Options.AuthenticationType);
            AuthenticationTicket ticket = new AuthenticationTicket(claimsIdentity, authProperties);
            return ticket;
        }

        private string GetUserId(string requestToken)
        {
            return _queryService.Query<GetUserIdForSyncServiceKey, string>(new GetUserIdForSyncServiceKey { SyncServiceKey = requestToken });
        }
    }

    public class SyncServiceAuthenticationOptions : AuthenticationOptions
    {
        public const string SyncServiceAuthenticationType = "SyncService";
        public SyncServiceAuthenticationOptions() : base(SyncServiceAuthenticationType)
        {
        }
    }
}