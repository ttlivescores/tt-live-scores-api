﻿namespace TTLiveScores.Web.Security
{
    public class TtLiveScoreClaimTypes
    {
        public const string Groups = "authorization.groups";
    }

    public class TtLiveScoreGroups
    {
        public const string Admin = "Admin";
    }
}