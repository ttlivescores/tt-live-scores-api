﻿using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TTLiveScores.Web.Security
{
    public class ClaimsAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string claimType;
        private readonly string claimValue;
        public ClaimsAuthorizeAttribute(string type, string value)
        {
            this.claimType = type;
            this.claimValue = value;
        }
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext filterContext)
        {
            var user = HttpContext.Current.User as ClaimsPrincipal;
            if (user.HasClaim(claimType, claimValue))
            {
                base.OnAuthorization(filterContext);
            }
            else if (user.HasClaim(c => c.Type == "app_metadata"))
            {
                if(!HasAppMetaDataClaim(user))
                    base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

        private bool HasAppMetaDataClaim(ClaimsPrincipal claimsPrincipal)
        {
            var appMetaData = claimsPrincipal.Claims.Single(x => x.Type == "app_metadata");
            var deserialized = JObject.Parse(appMetaData.Value);
            if (deserialized == null)
                return false;

            var tokens = deserialized.SelectTokens($"$..{claimType}");

            if (tokens == null)
                return false;

            return tokens.Any(token => token.Children().Any(c => c.ToString() == this.claimValue));
        }
    }
}