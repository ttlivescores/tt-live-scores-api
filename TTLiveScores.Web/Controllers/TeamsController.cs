﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Features.Teams;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Security;

namespace TTLiveScores.Web.Controllers
{
    public class TeamsController : ApiController
    {
        private readonly ICommandBus _bus;
        private readonly IQueryService _queryService;

        public TeamsController(ICommandBus bus, IQueryService queryService)
        {
            _bus = bus;
            _queryService = queryService;
        }

        [Route("api/teams/sync")]
        [HttpPost]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IHttpActionResult Sync()
        {
            _bus.Publish(new SyncTeams());
            return Ok();
        }

        [Route("api/teams")]
        [HttpDelete]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IHttpActionResult ClearTeams()
        {
            _bus.Publish(new ClearTeams());
            return Ok();
        }

        [Route("api/teams")]
        [HttpGet]
        public IEnumerable<Team> GetAllTeams()
        {
            return _queryService.Query<GetAllTeams, IEnumerable<Team>>(new GetAllTeams());
        } 
    }
}
