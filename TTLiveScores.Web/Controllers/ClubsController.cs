﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Security;
using WebApi.OutputCache.V2;

namespace TTLiveScores.Web.Controllers
{
    public class ClubsController : ApiController
    {
        private readonly ICommandBus _bus;
        private readonly IQueryService _queryService;

        public ClubsController(ICommandBus bus, IQueryService queryService)
        {
            _bus = bus;
            _queryService = queryService;
        }

        [Route("api/clubs/sync")]
        [HttpPost]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        //TODO make async
        public IHttpActionResult Sync()
        {
            _bus.Publish(new SyncClubs());
            return Ok();
        }

        [Route("api/clubs")]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 86400, ServerTimeSpan = 2592000, MustRevalidate = true)]
        public IEnumerable<Club> GetClubs ()
        {
            return _queryService.Query<GetAllClubs, IEnumerable<Club>>(new GetAllClubs());
        }
    }
}
