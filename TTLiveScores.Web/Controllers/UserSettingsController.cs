﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using TTLiveScores.Application.Contracts.Commands.UserSettings;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Application.Features.Players;
using TTLiveScores.Application.Features.Players.Queries;
using TTLiveScores.Application.Features.UserSettings;
using TTLiveScores.Application.Features.UserSettings.Commands;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Models;
using TTLiveScores.Web.Security;

namespace TTLiveScores.Web.Controllers
{
    [Authorize]
    public class UserSettingsController : ApiController
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly ILogger _logger;

        public UserSettingsController(ICommandBus commandBus, IQueryService queryService, ILogger logger)
        {
            _commandBus = commandBus;
            _queryService = queryService;
            _logger = logger;
        }

        [Route("api/users/my/club/{clubId}")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult ChangeUserClub(string clubId)
        {
            var command = new ChangeFavoriteClub { ClubId = clubId, UserId = User.Identity.GetUserId() };
            _commandBus.Publish(command);
            return Ok(clubId);
        }

        [Route("api/users/my/player/{playerId}")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult ChangeUserPlayerId(string playerId)
        {
            var player = _queryService.Query<GetPlayerById, Player>(new GetPlayerById(playerId));

            var clubCommand = new ChangeFavoriteClub { ClubId = player.Club.Id, UserId = User.Identity.GetUserId() };
            _commandBus.Publish(clubCommand);

            var playerCommand = new ChangePlayerId { PlayerId = playerId, UserId = User.Identity.GetUserId() };
            _commandBus.Publish(playerCommand);
            return Ok(playerId);
        }

        [Route("api/users/my/player/")]
        [HttpDelete]
        [Authorize]
        public IHttpActionResult RemoveUserPlayerId()
        {
            var command = new ChangePlayerId { PlayerId = null, UserId = User.Identity.GetUserId() };
            _commandBus.Publish(command);
            return Ok("removed");
        }

        [Route("api/users/my/club/")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult GetUserClub()
        {
            var query = new GetUserClub(User.Identity.GetUserId());
            var clubId = _queryService.Query<GetUserClub, string>(query);
            if (clubId == null)
                return NotFound();
            return Ok(clubId);
        }

        [Route("api/users/my/profile/")]
        [HttpGet]
        [Authorize]
        public IHttpActionResult GetUserProfile()
        {
            var userSettings =
                _queryService.Query<GetUserSettings, UserSettings>(new GetUserSettings(User.Identity.GetUserId()));

            var userProfile = new UserProfile
            {
                FavoriteClub = userSettings?.ClubId,
                PlayerId = userSettings?.PlayerId,
                License = userSettings?.SyncServiceKey
            };

            var identity = User.Identity as ClaimsIdentity;
            if (identity == null)
            {
                _logger.Warning($"No identity found sor user: {User.Identity}");
                return Ok(userProfile);
            }
            var name = identity.Claims.SingleOrDefault(c => c.Type == "name")?.Value;

            if (string.IsNullOrWhiteSpace(userSettings?.UserInfo.FullName))
            {
                _commandBus.Publish(new FillUserName(User.Identity.GetUserId(), name));
            }

            userProfile.ProposedPlayer = GetProposedPlayer(userProfile, name);
            
            return Ok(userProfile);
        }

        private ProposedPlayer GetProposedPlayer(UserProfile userProfile, string name)
        {
            if (!string.IsNullOrWhiteSpace(userProfile?.PlayerId))
                return null;

            _logger.Debug("user has no profile");
            _logger.Debug("Looking up profile for name:{0}", name);

            var foundPlayers = _queryService.Query<GetPlayersByName, IEnumerable<Player>>(new GetPlayersByName(name)).ToList();
            _logger.Debug("Found {0} players for name:{1}", foundPlayers.Count, name);
            if (foundPlayers.Count == 1)
            {
                var player = foundPlayers.Single();
                return new ProposedPlayer
                {
                    PlayerId = player.Id,
                    ClubId = player.Club.Id,
                    ClubName = player.Club.Name,
                    PlayerName = $"{player.FirstName} {player.LastName}",
                    Ranking = player.Rankings.Values.FirstOrDefault()
                };
            }
            return null;
        }

        [Authorize]
        [HttpPost]
        [Route("api/users/my/syncscoreslicense/activate")]
        public IHttpActionResult ActivateSyncScoreClient()
        {
            var requestKey = new RequestSyncServiceKey { UserId = User.Identity.GetUserId() };
            _commandBus.Publish(requestKey);

            return GetSyncScoresKey();
        }

        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        [HttpGet]
        [Route("api/users/syncscoreslicense")]
        public IHttpActionResult GetUsersLicenseKeys()
        {
            var userlicenses =
                _queryService.Query<GetUsersWithSyncScoresLicenses, IEnumerable<UserSettings>>(
                    new GetUsersWithSyncScoresLicenses());

            return Ok(userlicenses.Select(u => new { u.UserId, u.SyncServiceKey }));
        }

        [Authorize]
        [Route("api/users/my/syncscoreslicense/")]
        [HttpGet]
        public IHttpActionResult GetSyncScoresKey()
        {
            var userSettings =
                _queryService.Query<GetUserSettings, UserSettings>(new GetUserSettings(User.Identity.GetUserId()));
            return Ok(new { ClientKey = userSettings?.SyncServiceKey });
        }
    }
}
