﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Web.Security;

namespace TTLiveScores.Web.Controllers
{
    public class EventsController : ApiController
    {
        private readonly IEventRepository _eventRepository;


        public EventsController(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        [Route("api/events/latest")]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        [HttpGet]
        public IEnumerable<object> LatestEvents()
        {
            return _eventRepository.GetLatest();
        }

        // GET: Events
        [Route("api/events/{id}")]
        [HttpGet]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IEnumerable<object> Events(string id)
        {
            return _eventRepository.GetFromRevision(id, 0);
        }
    }
}
