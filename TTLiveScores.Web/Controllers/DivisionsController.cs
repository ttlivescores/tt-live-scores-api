﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Features.Divisions;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Security;

namespace TTLiveScores.Web.Controllers
{
    public class DivisionsController : ApiController
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;

        public DivisionsController(ICommandBus commandBus, IQueryService queryService)
        {
            _commandBus = commandBus;
            _queryService = queryService;
        }

        [HttpPost]
        [Route("api/divisions/sync")]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IHttpActionResult Sync()
        {
            _commandBus.Publish(new SyncDivisions());

            return Ok();
        }

        [HttpDelete]
        [Route("api/divisions")]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IHttpActionResult ClearDivisions()
        {
            _commandBus.Publish(new ClearDivisions());

            return Ok();
        }

        [Route("api/divisions")]
        [HttpGet]
        public IEnumerable<Division> GetDivisions()
        {
            return _queryService.Query<GetAllDivisions, IEnumerable<Division>>(new GetAllDivisions());
        }
    }
}
