﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Commands.Lineups;
using TTLiveScores.Application.Contracts.Lineups;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Framework.Utils;
using TTLiveScores.Web.Models;
using TTLiveScores.Web.Security;

namespace TTLiveScores.Web.Controllers
{
    public class ScoresController : ApiController
    {
        private readonly ICommandBus _bus;
        private readonly IQueryService _queryService;

        public ScoresController(ICommandBus bus, IQueryService queryService)
        {
            _bus = bus;
            _queryService = queryService;
        }

        [Authorize]
        [Route("api/matches/{matchId}/scores")]
        [HttpPost]
        public IHttpActionResult Post(string matchId, [FromBody] ScorePostModel score)
        {
            _bus.Publish(new UpdateScore { MatchId = matchId, HomeScore = score.HomeScore, AwayScore = score.AwayScore });

            return Ok(matchId);
        }

        [Authorize]
        [Route("api/matches/{matchId}/lineups")]
        [HttpPost]
        public IHttpActionResult Post(string matchId, [FromBody] LineUpPostModel lineupModel)
        {
            _bus.Publish(CreateTeamLineup(lineupModel.HomeLineups,matchId, true));
            _bus.Publish(CreateTeamLineup(lineupModel.AwayLineups,matchId, false));

            return Ok(matchId);
        }

        [Route("api/scores")]
        [HttpPost]
        public IHttpActionResult Post(IEnumerable<BulkScoreModel> scores)
        {

            foreach (var score in scores)
            {
                _bus.Publish(new UpdateScore { MatchId = score.MatchId, HomeScore = score.HomeScore, AwayScore = score.AwayScore });
            }

            return Ok();
        }

        [Route("api/lineups")]
        [HttpPost]
        public IHttpActionResult Post(IEnumerable<BulkLineupModel> lineups)
        {

            foreach (var lineup in lineups)
            {
                var fillHomeLineups = CreateTeamLineup(lineup, true);
                _bus.Publish(fillHomeLineups);

                var fillAwayLineups = CreateTeamLineup(lineup, false);
                _bus.Publish(fillAwayLineups);
            }

            return Ok();
        }

        private FillTeamLineup CreateTeamLineup(BulkLineupModel lineup, bool thuis)
        {
            return new FillTeamLineup
            {
                Home = thuis,
                MatchId = lineup.MatchId,
                LineupItems = lineup.LineupPlayers.Where(p=>p.IsHome == thuis)
                    .Select(x => new LineupItem
                {
                    
                    Position = x.Position,
                    PlayerId = x.PlayerId,
                }).ToList()
            };
        }

        private FillTeamLineup CreateTeamLineup(List<LineupPlayer> lineups, string matchId, bool thuis)
        {
            lineups = lineups ?? new List<LineupPlayer>();
            return new FillTeamLineup
            {
                Home = thuis,
                MatchId = matchId,
                LineupItems = lineups
                    .Select(x => new LineupItem
                    {
                        Position = x.Position,
                        PlayerId = x.PlayerId,
                        Rating = x.Ranking
                    }).ToList()
            };
        }

        [Route("api/clubs/{clubId}/scores")]
        [HttpGet]
        public IEnumerable<MatchScore> GetActiveScores(string clubId)
        {
            //todo optimize performance
            var progresses = _queryService.Query<GetClubMatchProgress, IEnumerable<MatchProgress>>(new GetClubMatchProgress(clubId))
                .ToDictionary(m => m.Id);

            var updatedByUserIds = progresses.Where(p=>!string.IsNullOrEmpty(p.Value.LastUpdateUserId)).Select(p => p.Value.LastUpdateUserId).Distinct();
            var updateUsers = _queryService.Query<GetUserNamesByIds, IDictionary<string, string>>(new GetUserNamesByIds(updatedByUserIds));

            var activeMatches = _queryService.Query<GetActiveMatches, IEnumerable<Match>>(new GetActiveMatches(clubId)).ToList();
            var activeMatchLineups = _queryService.Query<GetLineups, IEnumerable<MatchTeamLineup>>(new GetLineups(activeMatches.Select(m => m.Id))).ToList();
            
            var activeMatchScores = new List<MatchScore>();
            foreach (var match in activeMatches)
            {
                var matchScore = ToMatchScore(match);

                if (progresses.ContainsKey(match.Id))
                {
                    matchScore.HomeScore = progresses[matchScore.MatchId].HomeScore;
                    matchScore.AwayScore = progresses[matchScore.MatchId].AwayScore;
                    if(updateUsers.ContainsKey(progresses[matchScore.MatchId].LastUpdateUserId))
                        matchScore.LastUpdateUserFullName = updateUsers[progresses[matchScore.MatchId].LastUpdateUserId];
                }

                if (activeMatchLineups.Any(l => l.MatchId == matchScore.MatchId))
                {                    
                    matchScore.HomeLineup = MapLineups(activeMatchLineups, match.Id, match.HomeTeam.Id);
                    matchScore.AwayLineup = MapLineups(activeMatchLineups, match.Id, match.AwayTeam.Id);
                }
                activeMatchScores.Add(matchScore);
            }

            if ("test".Equals(clubId, StringComparison.InvariantCultureIgnoreCase))
                activeMatchScores.AddRange(activeMatchScores.Union(new[]
                {
                    new MatchScore {MatchId = "TST001", MatchStartDate = DateTime.Today.AddHours(19), HomeTeamClub = "TestHomeClub", HomeTeamLetter = "A", AwayTeamClub = "TestAwayClub", AwayTeamLetter = "A"},
                    new MatchScore {MatchId = "TST002", MatchStartDate = DateTime.Today.AddHours(19), HomeTeamClub = "TestHomeClub", HomeTeamLetter = "B", AwayTeamClub = "TestAwayClub", AwayTeamLetter = "B"},
                }));

            return activeMatchScores.OrderBy(m => m.MatchId);
        }

        private MatchScore ToMatchScore(Match m)
        {
            return new MatchScore{ MatchId = m.Id, MatchStartDate = m.Date.Value, HomeTeamClub = m.HomeTeam?.Club?.Name, HomeTeamClubId = m.HomeClub, AwayTeamClubId = m.AwayClub,  HomeTeamLetter = m.HomeTeam?.Letter, AwayTeamClub = m.AwayTeam?.Club?.Name, AwayTeamLetter = m.AwayTeam?.Letter };
        }

        private IEnumerable<MatchScore.Lineup> MapLineups(ICollection<MatchTeamLineup> activeMatchLineups, string matchId, string teamId)
        {
            var matchLineup = activeMatchLineups.SingleOrDefault(l=>l.MatchId == matchId && l.TeamId == teamId);
            if (matchLineup == null) yield break;

            foreach (var lineupItem in matchLineup.LineupItems)
            {
                yield return new MatchScore.Lineup {Position = lineupItem.Position, Ranking = lineupItem.Ranking, PlayerId = lineupItem.PlayerId};
            }
        }
    }
}
