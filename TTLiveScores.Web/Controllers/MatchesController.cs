﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Features.Matches.Commands;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Security;

namespace TTLiveScores.Web.Controllers
{
    public class MatchesController : ApiController
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;

        public MatchesController(ICommandBus commandBus, IQueryService queryService)
        {
            _commandBus = commandBus;
            _queryService = queryService;
        }

        [HttpPost]
        [Route("api/matches/sync")]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IHttpActionResult Sync()
        {
            _commandBus.Publish(new SyncMatches());

            return Ok();
        }

        [Route("api/matches")]
        [HttpDelete]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IHttpActionResult DeleteAllMatches()
        {
            _commandBus.Publish(new ClearMatchProgresses());
            _commandBus.Publish(new ClearMatches());

            return Ok();
        }

        [Route("api/matches")]
        [HttpGet]
        public IEnumerable<Match> GetAllMatches()
        {
            return _queryService.Query<GetAllMatches, IEnumerable<Match>>(new GetAllMatches());
        }

        [Route("api/clubs/{clubId}/matches")]
        [HttpGet]
        public IEnumerable<Match> GetClubMatches(string clubId)
        {
            return _queryService.Query<GetClubMatches, IEnumerable<Match>>(new GetClubMatches(clubId));
        }
    }
}
