﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TTLiveScores.Application.Features.Frenoy;
using TTLiveScores.Application.Features.Players;
using TTLiveScores.Application.Features.Players.Commands;
using TTLiveScores.Application.Features.Players.Queries;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;
using TTLiveScores.Web.Security;

namespace TTLiveScores.Web.Controllers
{
    public class PlayersController : ApiController
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;

        public PlayersController(ICommandBus commandBus, IQueryService queryService)
        {
            _commandBus = commandBus;
            _queryService = queryService;
        }

        [Route("api/clubs/{clubId}/players/")]

        public IEnumerable<Player> GetPlayers(string clubId)
        {
            return _queryService.Query<GetPlayers, IEnumerable<Player>>(new GetPlayers(clubId))
                .Where(p => int.TryParse(p.Indexes[IndexCategories.Men], out var rec))
                .OrderBy(p => p.Indexes.ContainsKey(IndexCategories.Men) ? IndexOrder(p.Indexes?[IndexCategories.Men]) : int.MaxValue)
                
                .ThenBy(p => p.LastName)
                .ThenBy(p => p.FirstName);
        }

        private int IndexOrder(string index)
        {
            var result = int.MaxValue;
            int.TryParse(index, out result);
            return result;
        }

        [HttpDelete]
        [Route("api/players")]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public IHttpActionResult ClearPlayers()
        {
            _commandBus.Publish(new ClearPlayers());

            return Ok();
        }

        [Route("api/players/sync")]
        [ClaimsAuthorize(TtLiveScoreClaimTypes.Groups, TtLiveScoreGroups.Admin)]
        public void FrenoyPlayerSync()
        {
            var frenoyPlayers = _queryService.Query<GetFrenoyPlayers, IEnumerable<FrenoyPlayer>>(new GetFrenoyPlayers());
            var commands = frenoyPlayers.Select(fp => new AddPlayer
            {
                PlayerId = fp.PlayerId,
                ClubId = fp.ClubId,
                Gender = ParseGender(fp.Gender),
                Status = ParseStatus(fp.Status),
                FirstName = fp.FirstName,
                LastName = fp.LastName,
                AgeCategory = fp.AgeCategory,
                RankingMen = fp.RankingMen,
                RankingFemale = fp.RankingFemale,
                IndexMen = fp.IndexMen,
                IndexWomen = fp.IndexWomen,
                IndexVeterans = fp.IndexVeterans,
                IndexYouth = fp.IndexYouth
            });

            foreach (var command in commands)
            {
                _commandBus.Publish(command);
            }
        }

        private Gender ParseGender(string gender)
        {
            return gender == "F" ? Gender.Female : Gender.Male;
        }

        private PlayerStatus ParseStatus(string status)
        {
            switch (status)
            {
                case "A": return PlayerStatus.Active;
                case "D": return PlayerStatus.Double;
                case "V": return PlayerStatus.V;
                case "L": return PlayerStatus.L;
                case "R": return PlayerStatus.Recreative;
                case "S": return PlayerStatus.S;
                case "T": return PlayerStatus.T;

                default: throw new ArgumentException($"Unknown status ParseStatus - {status}");
            }
        }
    }
}
