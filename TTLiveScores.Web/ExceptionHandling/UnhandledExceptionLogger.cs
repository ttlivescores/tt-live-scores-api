﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.ExceptionHandling;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Web.ExceptionHandling
{
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        private readonly ILogger _logger;

        public UnhandledExceptionLogger(ILogger logger)
        {
            _logger = logger;
        }

        public override void Log(ExceptionLoggerContext context)
        {
            _logger.Error(context.Exception,"Unhandled error in api");
        }
    }
}