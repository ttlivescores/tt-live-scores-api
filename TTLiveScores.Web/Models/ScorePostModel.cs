﻿namespace TTLiveScores.Web.Models
{
    public class ScorePostModel
    {
        public int HomeScore { get; set; }
        public int AwayScore { get; set; } 
    }
}