﻿using System.Collections.Generic;

namespace TTLiveScores.Web.Models
{
    public class BulkLineupModel
    {
        public string MatchId { get; set; }

        public IEnumerable<LineupPlayer> LineupPlayers { get; set; }
    }

    public class LineupPlayer
    {
        public string PlayerId { get; set; }

        public bool IsHome { get; set; }

        public int Position { get; set; }

        public string Ranking { get; set; }
        public bool Aanwezig { get; set; }
    }
}