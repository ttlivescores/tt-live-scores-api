﻿using System;
using System.Collections.Generic;

namespace TTLiveScores.Web.Models
{
    public class MatchScore
    {
        public string MatchId { get; set; }

        public DateTime MatchStartDate { get; set; }
        public string HomeTeamClub { get; set; }
        public string HomeTeamClubId { get; set; }
        public string AwayTeamClub { get; set; }
        public string AwayTeamClubId { get; set; }
        public string HomeTeamLetter { get; set; }
        public string AwayTeamLetter { get; set; }
        public int HomeScore { get; set; }
        public int AwayScore { get; set; }
        
        public string LastUpdateUserFullName { get; set; }

        public IEnumerable<Lineup> HomeLineup { get; set; }
        public IEnumerable<Lineup> AwayLineup { get; set; }

        public class Lineup
        {
            public int Position { get; set; }
            public string Ranking { get; set; }

            public string PlayerId { get; set; }
        }
    }
}