﻿namespace TTLiveScores.Web.Models
{
    public class UserProfile
    {
        public string FavoriteClub { get; set; }
        public string PlayerId { get; set; }

        public ProposedPlayer ProposedPlayer { get; set; }

        public string License { get; set; }
    }

    public class ProposedPlayer
    {
        public string PlayerId { get; set; }
        public string ClubId { get; set; }

        public string PlayerName { get; set; }

        public string Ranking { get; set; }

        public string ClubName { get; set; }
    }
}