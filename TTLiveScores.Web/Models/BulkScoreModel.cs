﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TTLiveScores.Web.Models
{
    public class BulkScoreModel
    {
        public string MatchId { get; set; }

        public int HomeScore { get; set; }

        public int AwayScore { get; set; }
    }
}