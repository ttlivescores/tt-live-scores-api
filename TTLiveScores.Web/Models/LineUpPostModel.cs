﻿using System.Collections.Generic;

namespace TTLiveScores.Web.Models
{
    public class LineUpPostModel
    {
        public List<LineupPlayer> HomeLineups { get; set; }
        public List<LineupPlayer> AwayLineups { get; set; }
    }
}