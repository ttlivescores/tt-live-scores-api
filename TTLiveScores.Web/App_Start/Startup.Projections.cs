﻿using Owin;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Web.DI;

namespace TTLiveScores.Web
{
    public partial class Startup
    {
        private ProjectionService projectionService;
        public void StartProjections(IAppBuilder app)
        {
			projectionService = CompositionRoot.Instance.CreateProjectionService();
			projectionService.StartProjections();
        }
    }
}