﻿namespace TTLiveScores.Integration.Contracts.Models.Frenoy
{
    public class Club
    {
        public string Id { get; set; }

        public string Name { get; set; } 
    }
}