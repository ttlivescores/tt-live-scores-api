﻿using System;

namespace TTLiveScores.Integration.Contracts.Models.Frenoy
{
    public class Fixture
    {
        public string MatchId { get; set; }
        public Levels Level { get; set; }
        public Categories Category { get; set; }
        public string MatchNr { get; set; }

        public DateTime? Date { get; set; }

        public string HomeTeamClub { get; set; }
        public string HomeTeamLetter { get; set; }

        public string AwayTeamClub { get; set; }
        public string AwayTeamLetter { get; set; }

        public int LeagueId { get; set; }

        public int LeagueNumber { get; set; }

        public string LeagueLetter { get; set; }
    }

    public enum Levels
    {
        Super = 6,
        Nationaal = 1,
        Landelijk = 2,
        //Brabant = 3,
        Henegouwen = 4,
        VlaamsBrabant = 5,
        OostVlaanderen=7,
        Antwerpen = 8,
        WestVlaanderen = 9,
        Limburg = 10,
        WaalsBrabant = 11,
        Namen = 12,
        Luik = 13,
        Luxemburg = 14
    }
    
    public enum Categories
    {
        Men = 1,
        Women = 2
    }
}