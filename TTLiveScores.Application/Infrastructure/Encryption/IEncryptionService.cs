﻿namespace TTLiveScores.Application.Helpers
{
    public interface IEncryptionService
    {
        string Encrypt(string valueToEncrypt);
    }
}