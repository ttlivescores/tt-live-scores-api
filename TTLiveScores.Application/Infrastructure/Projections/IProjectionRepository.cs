﻿namespace TTLiveScores.Application.Infrastructure.Projections
{
    public interface IProjectionRepository<TEntity>
    {
        void Insert(TEntity entity);

        void Update(TEntity entity);

        TEntity GetById(object id);

        void Clear();
    }
}