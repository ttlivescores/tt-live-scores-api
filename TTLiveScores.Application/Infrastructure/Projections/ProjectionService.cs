﻿using TTLiveScores.Framework.Eventstore;

namespace TTLiveScores.Application.Infrastructure.Projections
{
    public class ProjectionService
    {
        private readonly IEventPollingClient _pollingClient;

        public ProjectionService(IEventPollingClient pollingClient)
        {
            _pollingClient = pollingClient;
        }

        public void StartProjections()
        {
            _pollingClient.Start(null);
        }
    }
}