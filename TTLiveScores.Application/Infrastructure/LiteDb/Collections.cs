﻿namespace TTLiveScores.Application.Infrastructure.LiteDb
{
    public class DbLiteCollections
    {
        public const string MatchProgress = "matchprogress";
        public const string Clubs = "clubs";
        public const string Divisions = "divisions";
        public const string Teams = "teams";
        public const string Matches = "matches";
        public const string Lineups = "lineups";
        public const string UserSettings = "userSettings";
        public const string Players = "players";
    }
}