﻿using LiteDB;
using TTLiveScores.Application.Infrastructure.Projections;

namespace TTLiveScores.Application.Infrastructure.LiteDb
{
    public class LiteDbRepository<TEntity> : IProjectionRepository<TEntity> where TEntity : new()
    {
        private readonly LiteDatabase _db;
        private readonly string _collectionName;

        public LiteDbRepository(LiteDatabase db, string collectionName)
        {
            _db = db;
            _collectionName = collectionName;
        }

        public void Insert(TEntity entity)
        {
            _db.GetCollection<TEntity>(_collectionName).Insert(entity);
        }

        public void Update(TEntity entity)
        {
            _db.GetCollection<TEntity>(_collectionName).Update(entity);
        }

        public TEntity GetById(object id)
        {
            return _db.GetCollection<TEntity>(_collectionName).FindById(new BsonValue(id));
        }

        public void Clear()
        {
            _db.DropCollection(_collectionName);
        }
    }
}