﻿using System.IO;
using LiteDB;

namespace TTLiveScores.Application.Infrastructure.LiteDb
{
    public class SeasonDatabase : LiteDatabase
    {
        public SeasonDatabase(string connectionString) : base(connectionString)
        {
        }

        public SeasonDatabase(string connectionString, BsonMapper mapper = null) : base(connectionString, mapper)
        {
        }

        public SeasonDatabase(Stream stream, BsonMapper mapper = null) : base(stream, mapper)
        {
        }

        public SeasonDatabase(IDiskService diskService, BsonMapper mapper = null) : base(diskService, mapper)
        {
        }
    }
}