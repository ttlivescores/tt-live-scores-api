﻿namespace TTLiveScores.Application.Infrastructure.SignalR
{
    public class SignalREvent
    {
        public SignalREvent(object evt, string updateUser)
        {
            Event = evt;
            UpdateUser = updateUser;
        }
        public object Event { get; set; }

        public string EventType => Event.GetType().Name;

        public string UpdateUser { get; set; }
    }
}