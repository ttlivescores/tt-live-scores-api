﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Microsoft.AspNet.Identity;
using NEventStore;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Eventstore
{
    public class EventsRepository : IEventRepository
    {
        private readonly IStoreEvents _eventStore;
        private readonly ILogger _logger;

        public EventsRepository(IStoreEvents eventStore, ILogger logger)
        {
            _eventStore = eventStore;
            _logger = logger;
        }

        public IEnumerable<object> Get(string id, int maxRevision)
        {
            var commits = _eventStore.Advanced.GetFrom("default", id, int.MinValue, maxRevision);

            return
                from commit in commits
                from eventMessage in commit.Events
                select eventMessage.Body;
        }

        public IEnumerable<object> GetLatest()
        {
            var commits = _eventStore.Advanced.GetFrom("default", DateTime.Now.AddHours(-12));

            return
                from commit in commits
                from eventMessage in commit.Events
                orderby commit.CommitStamp descending 
                select new { eventMessage.Body, eventMessage.Headers, commit.CommitId, commit.CommitStamp, Type= eventMessage.Body.GetType() };
        }

        public IEnumerable<object> GetFromRevision(string id, int minRevision)
        {
            var commits = _eventStore.Advanced.GetFrom("default", id, minRevision, int.MaxValue);
            
            return
                from commit in commits
                from eventMessage in commit.Events
                select new {eventMessage.Body, eventMessage.Headers, commit.CommitId, commit.CommitStamp, Type = eventMessage.Body.GetType() };
        }

        public void PublishEvent(string streamId, object evt)
        {
            PublishEvents(streamId, new[] { evt });
        }

        public void PublishEvents(string streamId, IEnumerable<object> events)
        {
            using (var stream = _eventStore.OpenStream(streamId))
            {
                foreach (var msg in events.Select(evt => new EventMessage() { Body = evt, Headers = BuildMetadataHeaders() }))
                {
                    stream.Add(msg);
                    _logger.Debug("Adding event:{@0}", msg.Body);
                }
                stream.CommitChanges(Guid.NewGuid());
            }
        }
        
        private Dictionary<string, object> BuildMetadataHeaders()
        {
            var metadata = new Dictionary<string, object>();

            var currentUser = HttpContext.Current.User.Identity as ClaimsIdentity;
            var userEmail = currentUser?.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value ?? "unknown";

            metadata.Add(EventMetaData.User, userEmail);
            metadata.Add(EventMetaData.Userid, currentUser.GetUserId());
            
            return metadata;
        }
    }
    
}