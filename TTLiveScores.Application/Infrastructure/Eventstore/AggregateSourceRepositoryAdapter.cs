﻿using AggregateSource;

namespace TTLiveScores.Application.Infrastructure.Eventstore
{
    public class AggregateSourceRepositoryAdapter<TAggregate> : Framework.Eventstore.IRepository<TAggregate> where TAggregate : class, new()
    {
        private readonly IRepository<TAggregate> _repository;

        public AggregateSourceRepositoryAdapter(AggregateSource.IRepository<TAggregate> repository)
        {
            _repository = repository;
        }

        public void Add(string id, TAggregate aggregate)
        {
            _repository.Add(id, aggregate);
        }

        public TAggregate Get(string id)
        {
            return _repository.Get(id);
        }

        public TAggregate GetOptional(string id)
        {
            var optionalAggregate = _repository.GetOptional(id);
            return optionalAggregate.HasValue ? optionalAggregate.Value : null;
        }
        
    }
}