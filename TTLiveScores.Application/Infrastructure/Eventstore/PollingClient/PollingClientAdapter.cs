﻿using System;
using NEventStore;
using NEventStore.Client;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Eventstore.PollingClient
{
    public class PollingClientAdapter : IEventPollingClient
    {
        private readonly IObserver<ICommit> _commitObserverSubscriber;
        private readonly ILogger _logger;
        private readonly NEventStore.Client.PollingClient _pollingClient;
        private IObserveCommits _commitObserver;

        public PollingClientAdapter(IStoreEvents store, IObserver<ICommit> commitObserverSubscriber, ILogger logger)
        {
            _commitObserverSubscriber = commitObserverSubscriber;
            _logger = logger;
            _pollingClient = new NEventStore.Client.PollingClient(store.Advanced, 1000);
        }

        public async void Start(string checkPoint)
        {
            _logger.Debug("Starting observe from checkpoint {0}", checkPoint);
            _commitObserver = _pollingClient.ObserveFrom(checkPoint);

            using (new PollingHook(_commitObserver))
            {
                using (_commitObserver.Subscribe(_commitObserverSubscriber))
                {
                    await _commitObserver.Start();
                }
            }
        }

        public void Dispose()
        {
            _commitObserver?.Dispose();
        }
    }
}