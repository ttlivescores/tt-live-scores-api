﻿using System;
using NEventStore;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Eventstore.PollingClient
{
    public class EventBusDispatchCommitObserver : IObserver<ICommit>
    {
        private readonly IEventBus _bus;
        private readonly ILogger _logger;

        public EventBusDispatchCommitObserver(IEventBus bus, ILogger logger)
        {
            _bus = bus;
            _logger = logger;
        }

        public void OnCompleted()
        {

        }

        public void OnError(Exception error)
        {
            _logger.Error(error, "Error while polling for new events");
        }

        public void OnNext(ICommit commit)
        {
            if (DateTime.Now.AddDays(-1) > commit.CommitStamp) return;

            foreach (var evt in commit.Events)
            {
                _logger.Debug($"Dispatching event: {evt}");
                var metaData = new EventMetaData(evt.Headers, commit.CheckpointToken, commit.CommitStamp, commit.StreamRevision, commit.StreamId);
                _bus.Publish(evt.Body as IEvent, metaData);
            }

        }
    }
}