﻿using System.Linq;
using AggregateSource;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Eventstore
{
    public class EventStoreCommitHandler<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        private readonly ICommandHandler<TCommand> _handler;
        private readonly UnitOfWork _unitOfWork;
        private readonly IEventRepository _eventRepository;
        private readonly ILogger _logger;

        public EventStoreCommitHandler(ICommandHandler<TCommand> handler, UnitOfWork unitOfWork, IEventRepository eventRepository, ILogger logger)
        {
            _handler = handler;
            _unitOfWork = unitOfWork;
            _eventRepository = eventRepository;
            _logger = logger;
        }

        public void Execute(TCommand command)
        {
            _handler.Execute(command);  

            _logger.Debug("Saving changes of command:{@0}", command);

            foreach (var affected in _unitOfWork.GetChanges())
            {
                _eventRepository.PublishEvents(affected.Identifier, affected.Root.GetChanges());
                _logger.Debug("Committed changes of command:{@0}", command);
                affected.Root.ClearChanges();
            }
        }
    }
}