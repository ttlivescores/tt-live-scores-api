﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Bus
{
    public class ValidatedCommandBus : ICommandBus
    {
        private readonly ICommandBus _commandBus;
        private readonly ICollection<IValidator> _validators;

        public ValidatedCommandBus(ICommandBus commandBus, IEnumerable<IValidator> validators)
        {
            _commandBus = commandBus;
            _validators = validators.ToList();
        }
        
        public void Publish<TCommand>(TCommand command) where TCommand : ICommand
        {
            var validator = _validators.SingleOrDefault(v=>v.CanValidateInstancesOfType(command.GetType()));
            if (validator != null)
            {
                var result = validator.Validate(command);
                if(!result.IsValid)
                    throw new ValidationException(result.Errors);
            }
            _commandBus.Publish(command);
        }
    }
}