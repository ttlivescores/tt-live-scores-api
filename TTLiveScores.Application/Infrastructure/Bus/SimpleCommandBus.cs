﻿using System;
using System.Collections.Generic;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Bus
{
    public class SimpleCommandBus : ICommandBus
    {
        private readonly IDictionary<Type, Func<object>> _registry = new Dictionary<Type, Func<object>>();
        public void Register<TCommand>(Func<ICommandHandler<TCommand>> handlerAction) where TCommand : ICommand
        {
            _registry.Add((typeof(TCommand)), handlerAction);
        }

        public void Publish<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = (ICommandHandler<TCommand>)_registry[command.GetType()]();
            handler.Execute(command);
        }
    }
}