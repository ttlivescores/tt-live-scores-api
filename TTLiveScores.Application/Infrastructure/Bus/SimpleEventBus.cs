﻿using System;
using System.Collections.Generic;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Bus
{
    public class SimpleEventBus : IEventBus
    {
        private readonly ILogger _logger;
        private MultiValueDictionary<Type, Action<IEvent, EventMetaData>> _registry = new MultiValueDictionary<Type, Action<IEvent, EventMetaData>>();

        public SimpleEventBus(ILogger logger)
        {
            _logger = logger;
        }
        public void Register<TEvent>(Action<TEvent, EventMetaData> handlerAction) where TEvent : IEvent
        {
            _registry.Add(typeof(TEvent), (evt, metaData) => handlerAction((TEvent)evt, metaData));
        }

        public void Publish(IEvent evt)
        {
            Publish(evt, null);
        }

        public void Publish(IEvent evt, EventMetaData eventMetaData)
        {
            if (!_registry.ContainsKey(evt.GetType()))
            {
                _logger.Warning($"Commit checkpoint[{eventMetaData.Checkpoint}]: No eventhandler found for event: {evt} ");
                return;
            }

            foreach (var handler in _registry[evt.GetType()])
            {
                handler(evt, eventMetaData);
            }
        }
    }
}