﻿using System;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Infrastructure.Logging
{
    public class SerilogLoggerAdapter : ILogger
    {
        private readonly Serilog.ILogger _seriLogger;

        public SerilogLoggerAdapter(Serilog.ILogger seriLogger)
        {
            _seriLogger = seriLogger;
        }
        public void Verbose(string messageTemplate, params object[] propertyValues)
        {
            _seriLogger.Verbose(messageTemplate, propertyValues);
        }

        public void Debug(string messageTemplate, params object[] propertyValues)
        {
            _seriLogger.Debug(messageTemplate, propertyValues);
        }

        public void Information(string messageTemplate, params object[] propertyValues)
        {
            _seriLogger.Information(messageTemplate, propertyValues);
        }

        public void Warning(string messageTemplate, params object[] propertyValues)
        {
            _seriLogger.Warning(messageTemplate, propertyValues);
        }

        public void Error(string messageTemplate, params object[] propertyValues)
        {
            _seriLogger.Error(messageTemplate, propertyValues);
        }

        public void Error(Exception exception, string messageTemplate, params object[] propertyValues)
        {
            _seriLogger.Error(exception, messageTemplate, propertyValues);
        }
    }
}