﻿using System;
using System.Collections.Generic;

namespace TTLiveScores.Application.Helpers
{
    public static class EqualityComparers
    {
        public static IEqualityComparer<T> Create<T>(
                        Func<T, T, bool> equals,
                        Func<T, int> hash = null)
        {
            return new FuncEqualityComparer<T>(equals, hash ?? (t => 1));
        }

        private class FuncEqualityComparer<T> : EqualityComparer<T>
        {
            private readonly Func<T, T, bool> equals;
            private readonly Func<T, int> hash;

            public FuncEqualityComparer(Func<T, T, bool> equals, Func<T, int> hash)
            {
                this.equals = equals;
                this.hash = hash;
            }

            public override bool Equals(T a, T b)
            {
                return a == null
                    ? b == null
            : b != null && this.equals(a, b);
            }

            public override int GetHashCode(T obj)
            {
                return obj == null ? 0 : this.hash(obj);
            }
        }
    }
}