﻿using System.Collections.Generic;
using LiteDB;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Divisions
{
    public class DivisionQueryHandlers : IQueryHandler<GetAllDivisions, IEnumerable<Division>>
    {
        private readonly LiteDatabase _db;

        public DivisionQueryHandlers(LiteDatabase db)
        {
            _db = db;
        }

        public IEnumerable<Division> Query(GetAllDivisions query)
        {
            return _db.GetCollection<Division>(DbLiteCollections.Divisions).FindAll();
        }
    }
}