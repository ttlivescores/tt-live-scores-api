﻿using System;
using System.Collections.Generic;
using System.Linq;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Integration.Contracts.Models.Frenoy;
using TTLiveScores.Integration.Contracts.Queries;

namespace TTLiveScores.Application.Features.Divisions
{
    public class DivisionsCommandHandlers : ICommandHandler<SyncDivisions>
        , ICommandHandler<ClearDivisions>
    {
        private readonly IQueryService _queryService;
        private readonly IProjectionRepository<Division> _divisionRepository;

        public DivisionsCommandHandlers(IQueryService queryService, IProjectionRepository<Contracts.Models.Division> divisionRepository)
        {
            _queryService = queryService;
            _divisionRepository = divisionRepository;
        }

        public void Execute(SyncDivisions command)
        {
            var fixtures = _queryService.Query<GetFixtures, IEnumerable<Fixture>>(new GetFixtures());
            var divisions = fixtures.GroupBy(f => f.LeagueId)
                .Select(f => f.First())
                .Select(f => new Division { Id = f.LeagueId, Category = f.Category, Level = f.Level, LeagueNumber = f.LeagueNumber, LeagueLetter = f.LeagueLetter });

            foreach (var division in divisions)
            {
                var existingDivision = _divisionRepository.GetById(division.Id);
                if(existingDivision == null)
                    _divisionRepository.Insert(division);
                _divisionRepository.Update(division);
            }
        }

        public void Execute(ClearDivisions command)
        {
            _divisionRepository.Clear();
        }
    }
}