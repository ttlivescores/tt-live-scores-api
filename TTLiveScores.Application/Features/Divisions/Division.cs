﻿using TTLiveScores.Integration.Contracts.Models.Frenoy;

namespace TTLiveScores.Application.Contracts.Models
{
    public class Division
    {
        public int Id { get; set; }
        public Levels Level { get; set; }
        public Categories Category { get; set; }
        public int LeagueNumber { get; set; }

        public string LeagueLetter { get; set; }

        public string FullName
        {
            get
            {
                if (Level == Levels.Super)
                {
                    return $"Super - {Category}";
                }
                return $"{Level} {LeagueNumber}{LeagueLetter} - {Category}";
            }
        }

        public override string ToString()
        {
            return FullName;
        }
    }
}