﻿using System;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Features.Players.Commands;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Players
{
    public class PlayerCommandHandler : ICommandHandler<AddPlayer>, ICommandHandler<ClearPlayers>
    {
        private readonly IProjectionRepository<Player> _repository;

        public PlayerCommandHandler(IProjectionRepository<Player> repository)
        {
            _repository = repository;
        }

        public void Execute(ClearPlayers command)
        {
            _repository.Clear();
        }

        public void Execute(AddPlayer command)
        {
            var player = _repository.GetById(command.PlayerId);
            bool newPlayer = player == null;
            if (newPlayer)
            {
                player = new Player { Id = command.PlayerId };
            }

            player.Club = new Club(command.ClubId);
            player.FirstName = command.FirstName;
            player.LastName = command.LastName;
            player.AgeCategory = command.AgeCategory;
            player.Gender = command.Gender;
            player.Status = command.Status;

            player.AddIndex(IndexCategories.Men, command.IndexMen);
            player.AddIndex(IndexCategories.Women, command.IndexWomen);
            player.AddIndex(IndexCategories.Youth, command.IndexYouth);
            player.AddIndex(IndexCategories.Veterans, command.IndexVeterans);

            player.AddRanking(Gender.Male, command.RankingMen);
            player.AddRanking(Gender.Female, command.RankingFemale);


            if (newPlayer)
                _repository.Insert(player);
            else
                _repository.Update(player);

        }


    }
}