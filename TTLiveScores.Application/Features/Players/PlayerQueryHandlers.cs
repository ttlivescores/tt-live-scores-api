﻿using System.Collections.Generic;
using System.Linq;
using LiteDB;
using TTLiveScores.Application.Features.Players.Queries;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Players
{
    public class PlayerQueryHandlers : IQueryHandler<GetPlayers,IEnumerable<Player>>
        , IQueryHandler<GetPlayersByName, IEnumerable<Player>>
        , IQueryHandler<GetPlayerById, Player>
        , IQueryHandler<GetPlayersById, IEnumerable<Player>>
    {
        private readonly LiteDatabase _db;

        public PlayerQueryHandlers(LiteDatabase db)
        {
            _db = db;
        }

        public IEnumerable<Player> Query(GetPlayers query)
        {
            return _db.GetCollection<Player>(DbLiteCollections.Players).Find(p => p.Club.Id == query.ClubId);
        }

        public IEnumerable<Player> Query(GetPlayersByName query)
        {
            return _db.GetCollection<Player>(DbLiteCollections.Players)
                .Include(p=>p.Club)
                .Find(p => p.FullName == query.Name );
        }

        public Player Query(GetPlayerById query)
        {
            return _db.GetCollection<Player>(DbLiteCollections.Players).FindById(query.PlayerId);
        }

        public IEnumerable<Player> Query(GetPlayersById query)
        {
            var bsonArray = new BsonArray(query.Ids.Select(m => new BsonValue(m)));
            var bsonQuery = LiteDB.Query.In("_id", bsonArray);

            return _db.GetCollection<Player>(DbLiteCollections.Players).Find(bsonQuery);
        }
    }
}