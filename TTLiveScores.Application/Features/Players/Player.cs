﻿using System.Collections.Generic;
using TTLiveScores.Application.Contracts.Models;

namespace TTLiveScores.Application.Features.Players
{
    public class Player
    {
        public string Id { get; set; }

        public Club Club { get; set; }

        public Gender Gender { get; set; }

        public PlayerStatus Status { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}".ToLower();

        public string AgeCategory { get; set; }

        public IDictionary<Gender, string> Rankings { get; set; } = new Dictionary<Gender, string>();

        public IDictionary<IndexCategories, string> Indexes { get; set; } = new Dictionary<IndexCategories, string>();

        public void AddIndex(IndexCategories category, string index)
        {
            if (index != null && !Indexes.ContainsKey(category))
                Indexes.Add(category, index);
        }

        public void AddRanking(Gender gender, string ranking)
        {
            if (!string.IsNullOrWhiteSpace(ranking) && !Rankings.ContainsKey(gender))
                Rankings.Add(gender, ranking);
        }
    }

    public enum IndexCategories
    {
        Men,
        Women,
        Veterans,
        Youth
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum PlayerStatus
    {
        Active,
        Double,
        L,
        V,
        Recreative,
        S,
        T
    }
}