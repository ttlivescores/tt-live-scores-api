﻿namespace TTLiveScores.Application.Features.Players.Queries
{
    public class GetPlayers
    {
        public string ClubId { get; private set; }

        public GetPlayers(string clubId)
        {
            ClubId = clubId;
        }
    }
}