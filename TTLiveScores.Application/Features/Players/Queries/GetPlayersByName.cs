﻿namespace TTLiveScores.Application.Features.Players.Queries
{
    public class GetPlayersByName
    {
        public string Name { get; private set; }

        public GetPlayersByName(string name)
        {
            Name = name;
        }
    }
}