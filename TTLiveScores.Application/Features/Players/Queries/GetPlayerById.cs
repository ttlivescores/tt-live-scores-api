﻿namespace TTLiveScores.Application.Features.Players.Queries
{
    public class GetPlayerById
    {
        public GetPlayerById(string playerId)
        {
            PlayerId = playerId;
        }

        public string PlayerId { get; } 
    }
}