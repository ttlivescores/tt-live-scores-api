﻿using System.Collections.Generic;
using System.Linq;

namespace TTLiveScores.Application.Features.Players.Queries
{
    public class GetPlayersById
    {
        public IEnumerable<string> Ids { get; }

        public GetPlayersById(IEnumerable<string> ids)
        {
            Ids = ids?.Distinct() ?? new List<string>();
        }
    }
}