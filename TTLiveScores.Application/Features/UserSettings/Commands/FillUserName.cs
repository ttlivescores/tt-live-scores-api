﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.UserSettings.Commands
{
    public class FillUserName : ICommand    
    {
        public string UserId { get; private set; }
        public string UserName { get; private set; }

        public FillUserName(string userId, string userName)
        {
            UserId = userId;
            UserName = userName;
        }
    }
}