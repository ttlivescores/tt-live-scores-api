﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Commands.UserSettings
{
    public class ChangeFavoriteClub : ICommand
    {
        public string UserId { get; set; }
        public string ClubId { get; set; }
    }
}