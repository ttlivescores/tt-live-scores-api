﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Commands.UserSettings
{
    public class RequestSyncServiceKey:ICommand
    {
    public string UserId { get; set; }
    }
}