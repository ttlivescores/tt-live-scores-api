﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.UserSettings
{
    public class ChangePlayerId : ICommand
    {
        public string PlayerId { get; set; }
        public string UserId { get; set; }
    }
}