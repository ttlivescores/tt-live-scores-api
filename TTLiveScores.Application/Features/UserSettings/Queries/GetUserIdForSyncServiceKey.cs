﻿namespace TTLiveScores.Application.Contracts.Queries.UserSettings
{
    public class GetUserIdForSyncServiceKey
    {
        public string SyncServiceKey { get; set; }
    }
}