﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TTLiveScores.Application.Contracts.Queries.UserSettings
{
    public class GetUserNamesByIds
    {
        public IEnumerable<string> UserIds { get; }

        public GetUserNamesByIds(IEnumerable<string> userIds)
        {
            UserIds = userIds.Where(u => !string.IsNullOrWhiteSpace(u));
        }
    }
}