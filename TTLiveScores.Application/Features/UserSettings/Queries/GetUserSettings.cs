﻿namespace TTLiveScores.Application.Contracts.Queries.UserSettings
{
    public class GetUserSettings
    {
        public GetUserSettings(string userId)
        {
            UserId = userId;
        }

        public string UserId { get; private set; }
    }
}