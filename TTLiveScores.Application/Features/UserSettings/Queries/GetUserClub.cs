﻿namespace TTLiveScores.Application.Contracts.Queries.UserSettings
{
    public class GetUserClub
    {
        public GetUserClub(string userId)
        {
            UserId = userId;
        }

        public string UserId { get; set; } 
    }
}