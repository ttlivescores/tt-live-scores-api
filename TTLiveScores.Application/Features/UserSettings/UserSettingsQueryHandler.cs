﻿using System.Collections.Generic;
using System.Linq;
using LiteDB;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.UserSettings
{
    public class UserSettingsQueryHandler : IQueryHandler<GetUserClub,string>
        , IQueryHandler<GetUserIdForSyncServiceKey,string>
        , IQueryHandler<GetUserSettings, Contracts.Models.UserSettings>
        , IQueryHandler<GetUsersWithSyncScoresLicenses, IEnumerable<Contracts.Models.UserSettings>>
        , IQueryHandler<GetUserNamesByIds, IDictionary<string, string>>
    {
        private readonly LiteDatabase _db;

        public UserSettingsQueryHandler(LiteDatabase db)
        {
            _db = db;
        }
        public string Query(GetUserClub query)
        {
            return
                _db.GetCollection<Contracts.Models.UserSettings>(DbLiteCollections.UserSettings).FindById(query.UserId)?.ClubId;
        }

        public string Query(GetUserIdForSyncServiceKey query)
        {
            return
                _db.GetCollection<Contracts.Models.UserSettings>(DbLiteCollections.UserSettings)
                    .FindOne(u => u.SyncServiceKey == query.SyncServiceKey)?.UserId;
        }

        public Contracts.Models.UserSettings Query(GetUserSettings query)
        {
            return _db.GetCollection<Contracts.Models.UserSettings>(DbLiteCollections.UserSettings)
                .FindById(query.UserId);
        }

        public IEnumerable<Contracts.Models.UserSettings> Query(GetUsersWithSyncScoresLicenses query)
        {
            return _db.GetCollection<Contracts.Models.UserSettings>(DbLiteCollections.UserSettings)
                .Find(s => s.SyncServiceKey != null)
                .ToList().Where(s=>!string.IsNullOrWhiteSpace(s.SyncServiceKey));
        }

        public IDictionary<string,string> Query(GetUserNamesByIds query)
        {
            var bsonArray = new BsonArray(query.UserIds.Select(m => new BsonValue(m)));
            var bsonQuery = LiteDB.Query.In("_id", bsonArray);
            return
                _db.GetCollection<Contracts.Models.UserSettings>(DbLiteCollections.UserSettings)
                .Find(bsonQuery).ToDictionary(k=> k.UserId, v=>v.UserInfo.FullName);
        }
    }
}

            