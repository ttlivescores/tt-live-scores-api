﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.UserSettings.Events
{
    public class UserPlayerIdChanged : IEvent
    {
        public string UserId { get; set; }
        public string PlayerId { get; set; }
    }
}