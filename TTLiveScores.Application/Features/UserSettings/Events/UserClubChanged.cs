﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.UserSettings.Events
{
    public class UserClubChanged : IEvent
    {
        public string UserId { get; set; }
        public string ClubId { get; set; }
    }
}