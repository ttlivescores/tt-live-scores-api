﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.UserSettings.Events
{
    public class UserSyncServiceKeyActivated : IEvent
    {
        public string UserId { get; set; }

        public string SyncKey { get; set; }
    }
}