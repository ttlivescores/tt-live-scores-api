﻿using TTLiveScores.Application.Features.UserSettings;

namespace TTLiveScores.Application.Contracts.Models
{
    public class UserSettings
    {
        public string UserId { get; set; }

        public UserInfo UserInfo { get; set; } = new UserInfo();
        
        public string ClubId { get; set; }

        public string PlayerId { get; set; }
        
        public string SyncServiceKey { get; set; }
    }
}