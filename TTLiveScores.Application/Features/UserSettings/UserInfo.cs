﻿namespace TTLiveScores.Application.Features.UserSettings
{
    public class UserInfo
    {
        public string FullName { get; set; }
    }
}