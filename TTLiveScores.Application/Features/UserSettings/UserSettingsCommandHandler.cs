﻿using System;
using TTLiveScores.Application.Contracts.Commands.UserSettings;
using TTLiveScores.Application.Features.UserSettings.Commands;
using TTLiveScores.Application.Features.UserSettings.Events;
using TTLiveScores.Application.Helpers;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Features.UserSettings
{
    public class UserSettingsCommandHandler : ICommandHandler<ChangeFavoriteClub>, ICommandHandler<RequestSyncServiceKey>, ICommandHandler<FillUserName>
        , ICommandHandler<ChangePlayerId>
    {
        private readonly IEventRepository _eventRepository;
        private readonly IProjectionRepository<Contracts.Models.UserSettings> _userSettingsRepository;
        private readonly IEncryptionService _encryptionService;

        public UserSettingsCommandHandler(IEventRepository eventRepository, IProjectionRepository<Contracts.Models.UserSettings> userSettingsRepository, IEncryptionService encryptionService)
        {
            _eventRepository = eventRepository;
            _userSettingsRepository = userSettingsRepository;
            _encryptionService = encryptionService;
        }
        public void Execute(ChangeFavoriteClub command)
        {
            var userSettings = GetOrCreateUserSettings(command.UserId);
            if (userSettings.ClubId != command.ClubId)
            {
                userSettings.ClubId = command.ClubId;
                userSettings.PlayerId = null;

                _userSettingsRepository.Update(userSettings);
                _eventRepository.PublishEvent(command.UserId, new UserClubChanged() { UserId = userSettings.UserId, ClubId = userSettings.ClubId });
            }
        }

        private Contracts.Models.UserSettings GetOrCreateUserSettings(string userId)
        {
            var userSettings = _userSettingsRepository.GetById(userId);
            if (userSettings == null)
            {
                userSettings = new Contracts.Models.UserSettings { UserId = userId };
                _userSettingsRepository.Insert(userSettings);
            }
            return userSettings;
        }

        public void Execute(RequestSyncServiceKey command)
        {
            var userSettings = _userSettingsRepository.GetById(command.UserId);
            if (string.IsNullOrEmpty(userSettings?.ClubId))
                throw new Exception("Geen favoriete club geselecteerd");

            var syncKey = $"{userSettings.UserId}_{new Random().Next()}";
            var encryptedKey = _encryptionService.Encrypt(syncKey);
            userSettings.SyncServiceKey = encryptedKey;
            _userSettingsRepository.Update(userSettings);
            _eventRepository.PublishEvent(command.UserId, new UserSyncServiceKeyActivated { UserId = userSettings.UserId, SyncKey = encryptedKey });
        }

        public void Execute(ChangePlayerId command)
        {
            var userSettings = GetOrCreateUserSettings(command.UserId);
            userSettings.PlayerId = command.PlayerId;
            _userSettingsRepository.Update(userSettings);
            _eventRepository.PublishEvent(command.UserId, new UserPlayerIdChanged { UserId = userSettings.UserId, PlayerId = userSettings.PlayerId });
        }

        public void Execute(FillUserName command)
        {
            var userSettings = GetOrCreateUserSettings(command.UserId);
            userSettings.UserInfo.FullName = command.UserName;
            _userSettingsRepository.Update(userSettings);
        }
    }
}