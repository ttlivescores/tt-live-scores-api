﻿using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Frenoy.Players
{
    public class FrenoyPlayersQueryHandlers : IQueryHandler<GetFrenoyPlayers, IEnumerable<FrenoyPlayer>>
    {
        private readonly string _frenoyPlayersFile;

        public FrenoyPlayersQueryHandlers(string frenoyPlayersFile)
        {
            _frenoyPlayersFile = frenoyPlayersFile;
        }

        public IEnumerable<FrenoyPlayer> Query(GetFrenoyPlayers query)
        {
            using (TextFieldParser parser = new TextFieldParser(_frenoyPlayersFile))
            {
                parser.Delimiters = new[] { ";" };
                while (true)
                {
                    string[] parts = parser.ReadFields();
                    if (parts == null)
                    {
                        yield break;
                    }
                    yield return new FrenoyPlayer { ClubId  = parts[0], PlayerId = parts[1], Status = parts[2], LastName = parts[3], FirstName = parts[4], RankingMen = parts[5], RankingFemale = parts[6], Gender = parts[7], AgeCategory = parts[8], IndexMen = ParseOptional(parts[9]), IndexWomen = ParseOptional(parts[10]), IndexVeterans = ParseOptional(parts[11]), IndexYouth = ParseOptional(parts[12]) };
                }
            }
        }

        private string ParseOptional(string val)
        {
            return string.IsNullOrWhiteSpace(val) ? null : val;
        }
    }
}