﻿namespace TTLiveScores.Application.Features.Frenoy
{
    public class FrenoyPlayer
    {
        public string PlayerId { get; set; }

        public string ClubId { get; set; }

        public string Status { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public string AgeCategory { get; set; }

        public string RankingMen { get; set; }

        public string RankingFemale { get; set; }

        public string IndexMen { get; set; }

        public string IndexWomen { get; set; }

        public string IndexVeterans { get; set; }
        public string IndexYouth { get; set; }
    }
}