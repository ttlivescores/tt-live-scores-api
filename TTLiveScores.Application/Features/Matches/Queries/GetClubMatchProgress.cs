﻿namespace TTLiveScores.Application.Contracts.Queries
{
    public class GetClubMatchProgress
    {
        public GetClubMatchProgress(string clubId)
        {
            ClubId = clubId;
        }

        public string ClubId { get; private set; }
    }
}