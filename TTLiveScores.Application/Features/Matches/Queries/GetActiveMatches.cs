﻿namespace TTLiveScores.Application.Contracts.Queries
{
    public class GetActiveMatches
    {
        public GetActiveMatches(string clubId)
        {
            ClubId = clubId;
        }

        public string ClubId { get; private set; }
    }
}