﻿using System.Collections.Generic;

namespace TTLiveScores.Application.Contracts.Queries
{
    public class GetMatchProgesses
    {
        public GetMatchProgesses(IList<string> matchIds)
        {
            MatchIds = matchIds;
        }

        public IList<string> MatchIds { get; } 
    }
}