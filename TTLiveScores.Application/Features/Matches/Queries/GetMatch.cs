﻿namespace TTLiveScores.Application.Contracts.Queries
{
    public class GetMatch
    {
        public string MatchId { get; private set; }

        public GetMatch(string matchId)
        {
            MatchId = matchId;
        }
    }
}