﻿using System;

namespace TTLiveScores.Application.Contracts.Queries
{
    public class GetClubMatches
    {
        public GetClubMatches(string clubId)
        {
            ClubId = clubId;
        }

        public string ClubId { get; set; } 
    }
}