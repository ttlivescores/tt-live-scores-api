﻿using System;
using System.Collections.Generic;

namespace TTLiveScores.Application.Contracts.Models
{
    public class MatchTeamLineup
    {
        public Guid Id { get; set; }

        public string MatchId { get; set; }
        
        public string TeamId { get; set; }

        public ICollection<LineupItem> LineupItems { get; set; }

        public class LineupItem : IEquatable<LineupItem>
        {
            public int Position { get; set; }
            public string PlayerId { get; set; }
            public string Ranking { get; set; }

            public bool Equals(LineupItem other)
            {
                if (ReferenceEquals(null, other)) return false;
                if (ReferenceEquals(this, other)) return true;
                return Position == other.Position && string.Equals(PlayerId, other.PlayerId) && string.Equals(Ranking, other.Ranking);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (obj.GetType() != this.GetType()) return false;
                return Equals((LineupItem) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    var hashCode = Position;
                    hashCode = (hashCode * 397) ^ (PlayerId != null ? PlayerId.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (Ranking != null ? Ranking.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }
    }
}