﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Events
{
    public class MatchStarted : IEvent
    {
        public MatchStarted(string matchId)
        {
            MatchId = matchId;
        }
        public string MatchId { get; }
    }
}