﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Events
{
    public class ScoreChanged : IEvent
    {
        public string MatchId { get; set; }
        public int HomeScore { get; }
        public int AwayScore { get; }

        public ScoreChanged(string matchId, int homeScore, int awayScore)
        {
            MatchId = matchId;
            HomeScore = homeScore;
            AwayScore = awayScore;
        }
    }
}