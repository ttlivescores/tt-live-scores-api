﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Events
{
    public class ScoreCleared : IEvent
    {
        public string MatchId { get; }

        public ScoreCleared(string matchId)
        {
            MatchId = matchId;
        }
    }
}