﻿using System.Collections.Generic;
using System.Linq;
using LiteDB;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.MatchScores
{
    public class MatchProgressQueryHandlers : IQueryHandler<GetClubMatchProgress, IEnumerable<MatchProgress>>
        , IQueryHandler<GetMatchProgesses, IEnumerable<MatchProgress>>
        , IQueryHandler<GetAllMatchProgesses, IEnumerable<MatchProgress>>
    {
        private readonly LiteDatabase _db;

        public MatchProgressQueryHandlers(LiteDatabase db)
        {
            _db = db;
        }

        public IEnumerable<MatchProgress> Query(GetClubMatchProgress query)
        {
            return _db.GetCollection<MatchProgress>(DbLiteCollections.MatchProgress)
                .Find(m => m.HomeTeamClubId == query.ClubId || m.AwayTeamClubId == query.ClubId);
        }

        public IEnumerable<MatchProgress> Query(GetMatchProgesses query)
        {
            var bsonArray = new BsonArray(query.MatchIds.Select(m => new BsonValue(m)));
            var bsonQuery = LiteDB.Query.In("_id", bsonArray);

            return
                _db.GetCollection<MatchProgress>(DbLiteCollections.MatchProgress)
                .Find(bsonQuery);
        }

        public IEnumerable<MatchProgress> Query(GetAllMatchProgesses query)
        {
            return
                _db.GetCollection<MatchProgress>(DbLiteCollections.MatchProgress)
                    .FindAll();
        }
    }
}