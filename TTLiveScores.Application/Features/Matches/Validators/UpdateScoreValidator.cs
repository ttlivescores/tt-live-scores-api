﻿using FluentValidation;
using FluentValidation.Results;
using TTLiveScores.Application.Contracts.Commands;

namespace TTLiveScores.Application.Features.MatchScores.Validators
{
    public class UpdateScoreValidator : AbstractValidator<UpdateScore>
    {
        public UpdateScoreValidator()
        {
            RuleFor(s => s.MatchId).NotEmpty();

            RuleFor(s => s.HomeScore).GreaterThanOrEqualTo(0);
            RuleFor(s => s.AwayScore).GreaterThanOrEqualTo(0);

            Custom(r => r.HomeScore + r.AwayScore > 16 ? new ValidationFailure("Score", "Foutieve score") : null);
        }

    }
}