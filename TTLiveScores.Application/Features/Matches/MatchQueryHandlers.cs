﻿using System;
using System.Collections.Generic;
using System.Linq;
using LiteDB;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.MatchScores
{
    public class MatchQueryHandlers : IQueryHandler<GetAllMatches, IEnumerable<Match>>
        , IQueryHandler<GetActiveMatches, IEnumerable<Match>>
        , IQueryHandler<GetClubMatches, IEnumerable<Match>>
        , IQueryHandler<GetMatch, Match>

    {
        private readonly LiteDatabase _db;

        public MatchQueryHandlers(LiteDatabase db)
        {
            _db = db;
        }
        public IEnumerable<Match> Query(GetAllMatches query)
        {
            return _db.GetCollection<Match>(DbLiteCollections.Matches)
                .FindAll();
        }

        public IEnumerable<Match> Query(GetActiveMatches query)
        {
            var dateQuery = LiteDB.Query.Between("Date", DateTime.Today.AddDays(-1), DateTime.Today.AddDays(7));
            var clubQuery = LiteDB.Query.Or(LiteDB.Query.EQ("HomeClub", query.ClubId),
                LiteDB.Query.EQ("AwayClub", query.ClubId));
            var matchesFilter = LiteDB.Query.And(dateQuery, clubQuery);

            var matches = _db.GetCollection<Match>(DbLiteCollections.Matches)
                .Include(m => m.HomeTeam)
                .Include(m => m.HomeTeam.Club)
                .Include(m => m.AwayTeam)
                .Include(m => m.AwayTeam.Club)
                .Find(matchesFilter).ToList();
            
            return FilterUpcommingMatches(matches);
        }

        private IEnumerable<Match> FilterUpcommingMatches(IList<Match> matches )
        {
            var todaysOrYesterdaysMatches = FilterTodaysOrYesterdaysMatches(matches).ToList();
            return todaysOrYesterdaysMatches.Any()? todaysOrYesterdaysMatches : FilterUpcommingMatchesFollowingWeek(matches);
        }

        private IEnumerable<Match> FilterUpcommingMatchesFollowingWeek(IList<Match> matches)
        {
            for (int days = 0; days < 8; days++)
            {
                var dayMatches = matches.Where(m => m.Date.HasValue && DateTime.Today.AddDays(days).Date == m.Date.Value.Date).ToList();
                if(dayMatches.Any())
                    return dayMatches;
            }
            return Enumerable.Empty<Match>();
        }

        private IEnumerable<Match> FilterTodaysOrYesterdaysMatches(IList<Match> matches)
        {
            var directMatches = FindMatchesInTimeFrame(matches, 6, 18);
            return directMatches.Any() ? directMatches : FindMatchesInTimeFrame(matches, 12, 18);
        }

        private ICollection<Match> FindMatchesInTimeFrame(IList<Match> matches, int hoursBeforeMatch, int hoursAfterMatchStart)
        {
            return matches.Where(
                    m =>
                        m.Date.HasValue && m.Date.Value.AddHours(-hoursBeforeMatch) < DateTime.Now && m.Date.Value.AddHours(hoursAfterMatchStart) > DateTime.Now).ToList();
        } 

        public Match Query(GetMatch query)
        {
            return _db.GetCollection<Match>(DbLiteCollections.Matches)
                .Include(m => m.HomeTeam)
                .Include(m => m.HomeTeam.Club)
                .Include(m => m.AwayTeam)
                .Include(m => m.AwayTeam.Club)
                .FindOne(m => m.Id == query.MatchId);

        }

        public IEnumerable<Match> Query(GetClubMatches query)
        {
            var clubQuery = LiteDB.Query.Or(LiteDB.Query.EQ("HomeClub", query.ClubId),
                LiteDB.Query.EQ("AwayClub", query.ClubId));

            return _db.GetCollection<Match>(DbLiteCollections.Matches)
                .Include(m => m.HomeTeam)
                .Include(m => m.HomeTeam.Club)
                .Include(m => m.AwayTeam)
                .Include(m => m.AwayTeam.Club)
                .Find(clubQuery);
        }
    }
}