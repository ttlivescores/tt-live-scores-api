﻿using System.Security.Claims;
using Microsoft.AspNet.Identity;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Contracts.Queries.UserSettings;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.MatchScores
{
    public class MatchProgressCommandAuthorizationHandlers : ICommandAuthorizationHandler<UpdateScore>
    {
        private readonly IQueryService _queryService;

        public MatchProgressCommandAuthorizationHandlers(IQueryService queryService )
        {
            _queryService = queryService;
        }
        public bool IsAuthorized(UpdateScore command, ClaimsIdentity user)
        {
            //if (user?.GetUserId() == null) return false;

            var match = _queryService.Query<GetMatch, Match>(new GetMatch(command.MatchId));
            if (match == null) return true; //used for test matches
            var userClub = _queryService.Query<GetUserClub, string>(new GetUserClub(user.GetUserId()));
            
            return match.HomeClub == userClub || match.AwayClub == userClub;
        }
    }
}