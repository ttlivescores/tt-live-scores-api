﻿using System;
using System.Collections.Generic;
using System.Linq;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Features.Matches.Commands;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Integration.Contracts.Models.Frenoy;
using TTLiveScores.Integration.Contracts.Queries;
using Club = TTLiveScores.Application.Contracts.Models.Club;

namespace TTLiveScores.Application.Features.MatchScores
{
    public class MatchCommandHandlers : ICommandHandler<SyncMatches>
        , ICommandHandler<ClearMatches>
    {
        private readonly IQueryService _queryService;
        private readonly IProjectionRepository<Match> _matchRepository;

        public MatchCommandHandlers(IQueryService queryService, IProjectionRepository<Match> matchRepository)
        {
            _queryService = queryService;
            _matchRepository = matchRepository;
        }

        public void Execute(SyncMatches command)
        {
            var fixtures = _queryService.Query<GetFixtures, IEnumerable<Fixture>>(new GetFixtures());
            var matches = fixtures.Select(f => new Match()
            {
                Id = f.MatchNr.Replace("/", ""),
                Level = f.Level,
                MatchNr = f.MatchNr,
                FrenoyMatchId = f.MatchId,
                Category = f.Category,
                Date = f.Date,
                Division = new Division {Id = f.LeagueId},
                HomeTeam = new Team(new Club(f.HomeTeamClub), f.HomeTeamLetter, new Division() {Id = f.LeagueId}),
                AwayTeam = new Team(new Club(f.AwayTeamClub), f.AwayTeamLetter, new Division() {Id = f.LeagueId})
            });

            foreach (var match in matches)
            {
                var existingMatch = _matchRepository.GetById(match.Id);
                if (existingMatch == null)
                {
                    _matchRepository.Insert(match);
                }
                else
                {
                    _matchRepository.Update(match);
                }                
            }
        }

        public void Execute(ClearMatches command)
        {
            _matchRepository.Clear();
        }
    }
}