﻿using System;
using TTLiveScores.Integration.Contracts.Models.Frenoy;

namespace TTLiveScores.Application.Contracts.Models
{
    public class Match
    {
        private Team _homeTeam;
        private Team _awayTeam;

        public string Id { get; set; }

        public DateTime? Date { get; set; }

        public Team HomeTeam
        {
            get { return _homeTeam; }
            set
            {
                _homeTeam = value;
                if(_homeTeam?.Club?.Id != null)
                    HomeClub = _homeTeam.Club.Id;
            }
        }

        public Team AwayTeam
        {
            get { return _awayTeam; }
            set
            {
                _awayTeam = value;
                if (_awayTeam?.Club?.Id != null)
                    AwayClub = _awayTeam.Club.Id;
            }
        }

        public Levels Level { get; set; }
        public Categories Category { get; set; }
        public string MatchNr { get; set; }

        public string FrenoyMatchId { get; set; }

        public Division Division { get; set; }

        public string HomeClub { get; set; }
        public string AwayClub { get; set; }
    }
}