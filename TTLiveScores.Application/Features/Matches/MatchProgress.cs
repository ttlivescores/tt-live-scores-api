﻿namespace TTLiveScores.Application.Contracts.Models
{
    public class MatchProgress
    {
        public string Id { get; set; }

        public string HomeTeamClubId { get; set; }

        public string AwayTeamClubId { get; set; }

        public int HomeScore { get; set; }

        public int AwayScore { get; set; }

        public string LastUpdateUserId { get; set; }
    }
}