﻿using System;
using TTLiveScores.Application.Contracts.Events;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Features.Matches.Commands;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;

namespace TTLiveScores.Application.Features.MatchScores
{
    public class MatchProgressProjections : IEventHandler<ScoreChanged>
        , IEventHandler<ScoreCleared>
    {
        private readonly IProjectionRepository<MatchProgress> _repository;
        private readonly IQueryService _queryService;

        public MatchProgressProjections(IProjectionRepository<MatchProgress> repository, IQueryService queryService)
        {
            _repository = repository;
            _queryService = queryService;
        }

        public void Handle(ScoreChanged evt, EventMetaData eventMetaData)
        {
            var matchProgress = _repository.GetById(evt.MatchId) ?? CreateNewMatchProgress(evt.MatchId);
            if (matchProgress == null)
            {
                return;
            }

            matchProgress.HomeScore = evt.HomeScore;
            matchProgress.AwayScore = evt.AwayScore;
            matchProgress.LastUpdateUserId = eventMetaData.Headers?.ContainsKey("userId") == true ? eventMetaData.Headers["userId"]?.ToString() : null;
            
            _repository.Update(matchProgress);
        }

        private MatchProgress CreateNewMatchProgress(string id)
        {
            var matchProgress = new MatchProgress { Id = id };

            var match = _queryService.Query<GetMatch, Match>(new GetMatch(id));
            if (match == null) return null;

            matchProgress.HomeTeamClubId = match.HomeTeam?.Club?.Id;
            matchProgress.AwayTeamClubId = match.AwayTeam?.Club?.Id;

            _repository.Insert(matchProgress);
            return matchProgress;
        }
        
        public void Handle(ScoreCleared evt, EventMetaData eventMetaData)
        {
            var matchProgress = new MatchProgress { Id = evt.MatchId };
            matchProgress.AwayScore = 0;
            matchProgress.HomeScore = 0;
            _repository.Update(matchProgress);
        }
    }
}