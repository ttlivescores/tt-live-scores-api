﻿using System;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Commands
{
    public class RegisterMatch : ICommand
    {
        public string MatchId { get; set; }
        public int DivisionId { get; set; }
        public string HomeTeamId { get; set; }
        public string AwayTeamId { get; set; }
        public DateTime MatchDate { get; set; }
    }
}