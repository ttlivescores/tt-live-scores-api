﻿

using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Commands
{
    public class UpdateScore : ICommand
    {
        public string MatchId { get; set; }

        public int HomeScore { get; set; }

        public int AwayScore { get; set; }
    }
}