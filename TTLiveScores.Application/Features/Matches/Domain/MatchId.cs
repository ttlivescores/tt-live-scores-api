﻿namespace TTLiveScores.Application.Features.MatchScores.Domain
{
    public class MatchId
    {
        public string Id { get; }

        public MatchId(string matchId)
        {
            Id = matchId;
        }

        public static implicit operator string(MatchId matchId)
        {
            return matchId.Id;
        }
    }
}