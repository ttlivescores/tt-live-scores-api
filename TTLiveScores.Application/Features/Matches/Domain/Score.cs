﻿using System;

namespace TTLiveScores.Application.Features.MatchScores.Domain
{
    public class Score : IEquatable<Score>
    {
        public int HomeScore { get; }
        public int AwayScore { get; }

        public Score(int homeScore, int awayScore)
        {
            HomeScore = homeScore;
            AwayScore = awayScore;
        }

        public bool Equals(Score other)
        {
            if (ReferenceEquals(null, other)) return false;
            return HomeScore == other.HomeScore && AwayScore == other.AwayScore;
        }

        public override bool Equals(object other)
        {
            return Equals(other as Score);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (HomeScore*397) ^ AwayScore;
            }
        }
    }
}