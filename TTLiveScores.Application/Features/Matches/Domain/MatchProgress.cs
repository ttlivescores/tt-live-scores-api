﻿using AggregateSource;
using TTLiveScores.Application.Contracts.Events;

namespace TTLiveScores.Application.Features.MatchScores.Domain
{
    public class MatchProgress : AggregateRootEntity
    {
        private MatchId _matchId;
        private Score _score;
        public static MatchProgress StartMatch(MatchId matchId)
        {
            return new MatchProgress(new MatchStarted(matchId));
        }

        public MatchProgress()
        {
            Register<MatchStarted>(_ =>
            {
                _matchId = new MatchId(_.MatchId); _score = new Score(0, 0);
            });
            Register<ScoreChanged>(_
                =>
            {
                _matchId = _matchId ?? new MatchId(_.MatchId);
                _score = new Score(_.HomeScore, _.AwayScore);
            });

            Register<ScoreCleared>(_
                =>
            {
                _matchId = _matchId ?? new MatchId(_.MatchId);
                _score = new Score(0, 0);
            });

        }

        MatchProgress(MatchStarted matchStarted) : this()
        {
            ApplyChange(matchStarted);
        }

        public void ChangeScore(Score score)
        {
            if(score != null && !Equals(score, _score))
                ApplyChange(new ScoreChanged(_matchId, score.HomeScore, score.AwayScore));
        }

        public void FinalizeMatch(Score score)
        {

        }

        public void ClearScore()
        {
            ApplyChange(new ScoreCleared(_matchId));
        }
    }
}