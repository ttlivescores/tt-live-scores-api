﻿using System;
using System.Collections.Generic;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Commands.Lineups;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Features.Matches.Commands;
using TTLiveScores.Application.Features.MatchScores.Domain;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;
using MatchProgress = TTLiveScores.Application.Features.MatchScores.Domain.MatchProgress;

namespace TTLiveScores.Application.Features.MatchScores
{
    public class MatchProgressCommandHandlers : ICommandHandler<UpdateScore>
        , ICommandHandler<ClearMatchProgresses>
    {
        private readonly IRepository<MatchProgress> _matchProgressRepository;
        private readonly IQueryService _queryService;

        public MatchProgressCommandHandlers(IRepository<MatchProgress> matchProgressRepository, IQueryService queryService)
        {
            _matchProgressRepository = matchProgressRepository;
            _queryService = queryService;
        }


        public void Execute(UpdateScore command)
        {
            var strippedMatchId = command.MatchId.Replace("/","").Replace("-","");
            var matchProgress = _matchProgressRepository.GetOptional(strippedMatchId);

            if (matchProgress == null)
            {
                matchProgress = MatchProgress.StartMatch(new MatchId(strippedMatchId));
                _matchProgressRepository.Add(strippedMatchId, matchProgress);
            }

            matchProgress.ChangeScore(new Score(command.HomeScore, command.AwayScore));
        }

        public void Execute(ClearMatchProgresses command)
        {
            var inProgressMatches =
                _queryService.Query<GetAllMatchProgesses, IEnumerable<TTLiveScores.Application.Contracts.Models.MatchProgress>>(new GetAllMatchProgesses());

            foreach (var inProgressMatch in inProgressMatches)
            {
                var matchProgress =_matchProgressRepository.GetOptional(inProgressMatch.Id);
                matchProgress.ClearScore();
            }
        }
    }
}