﻿using System.Collections.Generic;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Integration.Contracts.Models.Frenoy;
using TTLiveScores.Integration.Contracts.Queries;

namespace TTLiveScores.Application.Features.Clubs
{
    public class ClubsCommandHandlers : ICommandHandler<SyncClubs>
    {
        private readonly IQueryService _queryService;
        private readonly IProjectionRepository<Contracts.Models.Club> _clubRepository;

        public ClubsCommandHandlers(IQueryService queryService, IProjectionRepository<Contracts.Models.Club> clubRepository )
        {
            _queryService = queryService;
            _clubRepository = clubRepository;
        }

        public void Execute(SyncClubs command)
        {
            var frenoyClubs = _queryService.Query<GetClubs, IEnumerable<Club>>(new GetClubs());

            foreach (var frenoyClub in frenoyClubs)
            {
                var club = _clubRepository.GetById(frenoyClub.Id);
                if (club == null)
                {
                    club = new Contracts.Models.Club
                    {
                        Id = frenoyClub.Id,
                        Name = frenoyClub.Name
                    };
                    _clubRepository.Insert(club);
                }
                else
                {
                    club.Name = frenoyClub.Name;
                    _clubRepository.Update(club);
                }
            }
        }
    }
}