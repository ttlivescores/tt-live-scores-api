﻿using System.Collections.Generic;
using LiteDB;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Clubs
{
    public class ClubQueryHandlers : IQueryHandler<GetAllClubs, IEnumerable<Club>>

    {
        private readonly LiteDatabase _db;

        public ClubQueryHandlers(LiteDatabase db)
        {
            _db = db;
        }

        public IEnumerable<Club> Query(GetAllClubs query)
        {
            return _db.GetCollection<Club>(DbLiteCollections.Clubs).FindAll();
        }
    }
}