﻿namespace TTLiveScores.Application.Contracts.Models
{
    public class Club
    {
        public Club()
        {
            
        }

        public Club(string id)
        {
            Id = id;
        }

        public string Id { get; set; }
        public string Name { get; set; } 
    }
}