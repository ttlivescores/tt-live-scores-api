﻿using System.Collections.Generic;
using LiteDB;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Teams
{
    public class TeamQueryHandlers : IQueryHandler<GetAllTeams,IEnumerable<Team>>
    {
        private readonly LiteDatabase _db;

        public TeamQueryHandlers(LiteDatabase db )
        {
            _db = db;
        }

        public IEnumerable<Team> Query(GetAllTeams query)
        {
            return _db.GetCollection<Team>(DbLiteCollections.Teams).FindAll();
        }
    }
}