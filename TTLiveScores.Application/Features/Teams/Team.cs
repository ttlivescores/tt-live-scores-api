﻿namespace TTLiveScores.Application.Contracts.Models
{
    public class Team
    {
        private string _id;
        public Team()
        {

        }

        public Team(Club club, string letter, Division division)
        {
            Club = club;
            Letter = letter;
            Division = division;
        }

        public string Id
        {
            get
            {
                if (_id != null)
                    return _id;
                return $"{Club?.Id}_{Letter}_{Division?.Id}";
            }
            set { _id = value; }
        }

        public string Letter { get; set; }

        public Division Division { get; set; }

        public Club Club { get; set; }

        public string FullName => $"{Club?.Name} {Letter}";
    }


}