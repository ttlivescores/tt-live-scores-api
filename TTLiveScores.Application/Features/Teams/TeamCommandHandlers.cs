﻿using System;
using System.Collections.Generic;
using System.Linq;
using TTLiveScores.Application.Contracts.Commands;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Helpers;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Integration.Contracts.Models.Frenoy;
using TTLiveScores.Integration.Contracts.Queries;
using Club = TTLiveScores.Application.Contracts.Models.Club;

namespace TTLiveScores.Application.Features.Teams
{
    public class TeamCommandHandlers : ICommandHandler<SyncTeams>
        , ICommandHandler<ClearTeams>
    {
        private readonly IQueryService _queryService;
        private readonly IProjectionRepository<Team> _teamRepository;

        public TeamCommandHandlers(IQueryService queryService, IProjectionRepository<Contracts.Models.Team> teamRepository)
        {
            _queryService = queryService;
            _teamRepository = teamRepository;
        }


        public void Execute(SyncTeams command)
        {
            var fixtures = _queryService.Query<GetFixtures, IEnumerable<Fixture>>(new GetFixtures());
            var allTeams = fixtures.Select(f => new { HomeTeam = MapTeam(f, true), AwayTeam = MapTeam(f, false) }).ToList();

            var equalityComparer = EqualityComparers.Create<Team>((a, b) => a.Id.Equals(b.Id), t => t.Id.GetHashCode());
            var distinctTeams = allTeams.Select(t => t.HomeTeam).Distinct(equalityComparer)
                .Union(allTeams.Select(t => t.AwayTeam), equalityComparer);

            foreach (var distinctTeam in distinctTeams)
            {
                var existingTeam = _teamRepository.GetById(distinctTeam.Id);
                if (existingTeam == null)
                    _teamRepository.Insert(distinctTeam);
                else
                {
                    _teamRepository.Update(distinctTeam);
                }
            }
        }

        public void Execute(ClearTeams command)
        {
            _teamRepository.Clear();
        }
        private Team MapTeam(Fixture fixture, bool home)
        {
            return new Team
            {
                Letter = home ? fixture.HomeTeamLetter : fixture.AwayTeamLetter,
                Club = new Club { Id = home ? fixture.HomeTeamClub : fixture.AwayTeamClub },
                Division = new Division() { Id = fixture.LeagueId }
            };
        }
    }
}