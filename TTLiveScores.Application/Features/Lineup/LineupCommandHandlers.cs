﻿using System;
using System.Collections.Generic;
using System.Linq;
using TTLiveScores.Application.Contracts.Commands.Lineups;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Contracts.Queries;
using TTLiveScores.Application.Features.Lineup.Events;
using TTLiveScores.Application.Features.Players;
using TTLiveScores.Application.Features.Players.Queries;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;
using TTLiveScores.Framework.Infrastructure;

namespace TTLiveScores.Application.Features.Lineup
{
    public class LineupCommandHandlers : ICommandHandler<FillTeamLineup>
    {
        private readonly IProjectionRepository<MatchTeamLineup> _lineupsRepository;
        private readonly IQueryService _queryService;
        private readonly IEventRepository _eventbus;

        public LineupCommandHandlers(IProjectionRepository<MatchTeamLineup> lineupsRepository, IQueryService queryService, IEventRepository eventbus)
        {
            _lineupsRepository = lineupsRepository;
            _queryService = queryService;
            _eventbus = eventbus;
        }

        public void Execute(FillTeamLineup command)
        {
            var strippedMatchId = command.MatchId.Replace("/", "").Replace("-", "");
            var match = _queryService.Query<GetMatch, Match>(new GetMatch(strippedMatchId));

            if (match == null) return; //Move this line to validation

            var teamId = command.Home ? match.HomeTeam.Id : match.AwayTeam.Id;
            var matchLineup = _queryService.Query<GetLineup, MatchTeamLineup>(new GetLineup(strippedMatchId, teamId)) ?? new MatchTeamLineup
            {
                MatchId = strippedMatchId,
                TeamId = teamId,
            };

            var originalLineupItems = matchLineup.LineupItems?.ToList() ?? new List<MatchTeamLineup.LineupItem>();
            matchLineup.LineupItems = MapLineups(command.LineupItems).ToList();

            if (matchLineup.Id == Guid.Empty)
            {
                matchLineup.Id = Guid.NewGuid();
                _lineupsRepository.Insert(matchLineup);
            }
            else
            {
                _lineupsRepository.Update(matchLineup);
            }

            if (!originalLineupItems.SequenceEqual(matchLineup.LineupItems))
            {
                _eventbus.PublishEvent(strippedMatchId, new LineupChanged { MatchId = matchLineup.MatchId, TeamId = matchLineup.TeamId });
            }
        }

        private IEnumerable<MatchTeamLineup.LineupItem> MapLineups(ICollection<LineupItem> lineupItems)
        {
            FillRatingsForPlayers(lineupItems);

            int counter = 0;
            foreach (var commandLineupItem in lineupItems.OrderByDescending(li=>!string.IsNullOrWhiteSpace(li.Rating)).ThenBy(li => li.Rating))
            {
                yield return new MatchTeamLineup.LineupItem
                {
                    Position = counter++,
                    PlayerId = string.IsNullOrWhiteSpace(commandLineupItem.PlayerId) ? null: commandLineupItem.PlayerId,
                    Ranking = string.IsNullOrWhiteSpace(commandLineupItem.Rating) ? null : commandLineupItem.Rating
                };
            }
        }

        private void FillRatingsForPlayers(ICollection<LineupItem> lineupItems)
        {
            var lineupsMetPlayerId = lineupItems.Where(l => l.HasPlayerId).ToList();
            var playerIds = lineupsMetPlayerId.Select(li => li.PlayerId);
            var query = new GetPlayersById(playerIds);
            var players = _queryService.Query<GetPlayersById, IEnumerable<Player>>(query).ToDictionary(p => p.Id);

            foreach (var lineupItem in lineupsMetPlayerId)
            {
                if (players.ContainsKey(lineupItem.PlayerId))
                {
                    var player = players[lineupItem.PlayerId];
                    if (player.Rankings.ContainsKey(Gender.Male))
                    {
                        lineupItem.Rating = player.Rankings[Gender.Male];
                        if (lineupItem.Rating.Equals("NC"))
                        {
                            lineupItem.Rating = "NG";
                        }
                    }
                }
            }
        }
    }
}