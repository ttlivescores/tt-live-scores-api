﻿using System.Collections.Generic;
using System.Linq;

namespace TTLiveScores.Application.Contracts.Lineups
{
    public class GetLineups
    {
        public IEnumerable<string> MatchIds { get; }

        public GetLineups(IEnumerable<string> matchIds )
        {
            MatchIds = matchIds.ToList();
        }   
    }
}