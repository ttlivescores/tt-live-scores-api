﻿using System.Collections.Generic;
using System.Linq;
using LiteDB;
using TTLiveScores.Application.Contracts.Lineups;
using TTLiveScores.Application.Contracts.Models;
using TTLiveScores.Application.Infrastructure.LiteDb;
using TTLiveScores.Application.Infrastructure.Projections;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Lineup
{
    public class LineupQueryHandlers : IQueryHandler<GetLineups, IEnumerable<MatchTeamLineup>>
    , IQueryHandler<GetLineup, MatchTeamLineup>
    {
        private readonly LiteDatabase _db;


        public LineupQueryHandlers(LiteDatabase db)
        {
            _db = db;
        }



        public IEnumerable<MatchTeamLineup> Query(GetLineups query)
        {
            var bsonArray = new BsonArray(query.MatchIds.Select(m => new BsonValue(m)));
            return
                _db.GetCollection<MatchTeamLineup>(DbLiteCollections.Lineups)
                    ?.Find(LiteDB.Query.In("MatchId",bsonArray));
        }

        public MatchTeamLineup Query(GetLineup query)
        {
            return _db.GetCollection<MatchTeamLineup>(DbLiteCollections.Lineups)
                .FindOne(x => x.MatchId.Equals(query.MatchId) && x.TeamId.Equals(query.TeamId));
        }
    }
}