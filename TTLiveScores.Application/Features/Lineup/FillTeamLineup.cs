﻿using System.Collections.Generic;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Contracts.Commands.Lineups
{
    public class FillTeamLineup : ICommand
    {
        public string MatchId { get; set; }

        public bool Home { get; set; }

        public ICollection<LineupItem> LineupItems { get; set; }
    }

    public class LineupItem
    {
        public int Position { get; set; }
        public string PlayerId { get; set; }
        public string Rating { get; set; }

        public bool HasPlayerId => !string.IsNullOrWhiteSpace(PlayerId);
        public bool HasRating => !string.IsNullOrWhiteSpace(Rating);

    }
}