﻿namespace TTLiveScores.Application.Features.Lineup
{
    public class GetLineup
    {
        public string MatchId { get; }
        public string TeamId { get; }

        public GetLineup(string matchId, string teamId)
        {
            MatchId = matchId;
            TeamId = teamId;
        }
    }
}