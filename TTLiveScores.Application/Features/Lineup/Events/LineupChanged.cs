﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Application.Features.Lineup.Events
{
    public class LineupChanged : IEvent
    {
        public string MatchId { get; set; }
        public string TeamId { get; set; }
    }
}