﻿using System.Linq;
using TTLiveScores.Integration.Contracts.Queries;
using TTLiveScores.Integration.Frenoy.QueryHandlers;
using Xunit;

namespace TTLiveScores.Integration.Tests.Fixtures
{
    public class FixturesQueryHandlerTests
    {
        [Fact]
        public void TestGetFixtures()
        {
            var handler = new FixturesQueryHandlers("Fixtures/fixtures.txt");
            var fixtures = handler.Query(new GetFixtures()).ToList();

            Assert.NotEmpty(fixtures);
            Assert.Equal(44692, fixtures.Count());
        }
    }
}