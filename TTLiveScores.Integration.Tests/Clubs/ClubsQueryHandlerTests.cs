﻿using System.Linq;
using TTLiveScores.Integration.Contracts.Queries;
using TTLiveScores.Integration.Frenoy.QueryHandlers;
using Xunit;

namespace TTLiveScores.Integration.Tests.Clubs
{
    public class ClubsQueryHandlerTests
    {

        [Fact]
        public void TestGetClubs()
        {
            var handler = new ClubsQueryHandler("Clubs/clubs.txt");
            var clubs = handler.Query(new GetClubs());

            Assert.NotEmpty(clubs);
            Assert.Equal("000", clubs.First().Id);
        }
    }
}