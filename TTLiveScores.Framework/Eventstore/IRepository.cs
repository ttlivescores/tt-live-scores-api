﻿using System;

namespace TTLiveScores.Framework.Eventstore
{
    public interface IRepository<TAggregate> where TAggregate : class
    {
        void Add(string id, TAggregate aggregate);
        TAggregate Get(string id);
        TAggregate GetOptional(string id);
    }
}