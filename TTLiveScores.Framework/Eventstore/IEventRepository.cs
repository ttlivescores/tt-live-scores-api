﻿using System.Collections.Generic;

namespace TTLiveScores.Framework.Eventstore
{
    public interface IEventRepository
    {
        IEnumerable<object> Get(string streamId, int maxRevision);

        IEnumerable<object> GetLatest();
        IEnumerable<object> GetFromRevision(string id, int minRevision);

        void PublishEvent(string streamId, object evt);

        void PublishEvents(string streamId, IEnumerable<object> evt);
    }
}