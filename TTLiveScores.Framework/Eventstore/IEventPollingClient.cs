﻿namespace TTLiveScores.Framework.Eventstore
{
    public interface IEventPollingClient
    {
        void Start(string checkPoint);
    }
}