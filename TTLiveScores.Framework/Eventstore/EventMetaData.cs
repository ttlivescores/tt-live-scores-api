﻿using System;
using System.Collections.Generic;

namespace TTLiveScores.Framework.Eventstore
{
    public class EventMetaData
    {
        public const string Userid = "userId";
        public const string User = "user";

        public EventMetaData(IDictionary<string, object> headers, string checkpoint, DateTime commitDate, int revision, string streamId)
        {
            Headers = headers;
            Checkpoint = checkpoint;
            CommitDate = commitDate;
            Revision = revision;
            StreamId = streamId;
        }

        public IDictionary<string, object> Headers { get; private set; }
        public string Checkpoint { get; private set; }

        public DateTime CommitDate { get; private set; }
        public int Revision { get; private set; }

        public string StreamId { get; private set; }

        public string GetUserId()
        {
            return GetHeaderField(Userid);
        }

        public string GetUserEmail()
        {
            return GetHeaderField(User);
        }

        public string GetHeaderField(string key)
        {
            return Headers.ContainsKey(key) ? Headers[key]?.ToString() : null;
        }
    }
}