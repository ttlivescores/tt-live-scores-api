﻿using System.Security.Claims;

namespace TTLiveScores.Framework.Cqrs
{
    public interface ICommandAuthorizationHandler<in TCommand> where TCommand : ICommand
    {
        bool IsAuthorized(TCommand command, ClaimsIdentity user);
    }
}