﻿using System.Security.Cryptography.X509Certificates;

namespace TTLiveScores.Framework.Cqrs
{
    public interface IQueryService
    {
        TResult Query<TQuery, TResult>(TQuery query);
    }
}