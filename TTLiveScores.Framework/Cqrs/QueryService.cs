﻿using System;
using System.Collections.Generic;

namespace TTLiveScores.Framework.Cqrs
{
    public class QueryService : IQueryService
    {
        private readonly IDictionary<Tuple<Type,Type>, Func<object>> _registry = new Dictionary<Tuple<Type, Type>, Func<object>>();
        public void Register<TQuery, TResult>(Func<IQueryHandler<TQuery,TResult>> handlerAction)
        {
            _registry.Add( new Tuple<Type, Type>(typeof(TQuery), typeof(TResult)), handlerAction);
        }
        public TResult Query<TQuery, TResult>(TQuery query)
        {
            dynamic handler = _registry[new Tuple<Type, Type>(typeof (TQuery), typeof (TResult))]();

            return handler.Query(query);
        }
    }
}