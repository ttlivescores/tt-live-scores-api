﻿namespace TTLiveScores.Framework.Cqrs
{
    public interface IQueryHandler<TQuery, TResult>
    {
        TResult Query(TQuery query);
    }
}