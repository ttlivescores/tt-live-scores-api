﻿using TTLiveScores.Framework.Eventstore;

namespace TTLiveScores.Framework.Cqrs
{
    public interface IEventHandler<TEvent> where TEvent : IEvent
    {
        void Handle(TEvent evt, EventMetaData eventMetaData);
    }
}