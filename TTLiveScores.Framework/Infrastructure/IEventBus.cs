﻿using TTLiveScores.Framework.Cqrs;
using TTLiveScores.Framework.Eventstore;

namespace TTLiveScores.Framework.Infrastructure
{
    public interface IEventBus
    {
        void Publish(IEvent evt);
        void Publish(IEvent evt, EventMetaData eventMetaData);
    }
}