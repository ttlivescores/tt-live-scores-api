﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Framework.Infrastructure
{
    public class AuthorisedCommandBusDecorator : ICommandBus
    {
        private readonly ICommandBus _commandBus;

        public AuthorisedCommandBusDecorator(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        private readonly IDictionary<Type, Func<object>> _registry = new Dictionary<Type, Func<object>>();
        public void Register<TCommand>(Func<ICommandAuthorizationHandler<TCommand>> handlerAction) where TCommand : ICommand
        {
            _registry.Add((typeof(TCommand)), handlerAction);
        }
        public void Publish<TCommand>(TCommand command) where TCommand : ICommand
        {
            if (_registry.ContainsKey(command.GetType()))
            {
                var handler = (ICommandAuthorizationHandler<TCommand>)_registry[command.GetType()]();
                var user = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
                var authorized = handler.IsAuthorized(command, user);
                if (!authorized)
                    throw new Exception("Not authorized to execute command");
            }
            _commandBus.Publish(command);
        }
    }
}