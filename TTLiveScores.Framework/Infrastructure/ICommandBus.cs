﻿using TTLiveScores.Framework.Cqrs;

namespace TTLiveScores.Framework.Infrastructure
{
    public interface ICommandBus
    {
        void Publish<TCommand>(TCommand command) where TCommand : ICommand;
    }
}